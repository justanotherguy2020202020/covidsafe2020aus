package kotlinx.coroutines.flow.internal;

import java.util.concurrent.CancellationException;
import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.channels.ChannelIterator;
import kotlinx.coroutines.channels.ChannelsKt;
import kotlinx.coroutines.channels.ReceiveChannel;
import kotlinx.coroutines.channels.SendChannel;
import kotlinx.coroutines.flow.FlowCollector;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0004\b\u0002\u0010\u0004*\u00020\u0005H@¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"<anonymous>", "", "T1", "T2", "R", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "kotlinx/coroutines/flow/internal/CombineKt$zipImpl$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Combine.kt */
final class CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    final /* synthetic */ FlowCollector $this_unsafeFlow;
    Object L$0;
    Object L$1;
    Object L$10;
    Object L$11;
    Object L$12;
    Object L$2;
    Object L$3;
    Object L$4;
    Object L$5;
    Object L$6;
    Object L$7;
    Object L$8;
    Object L$9;
    int label;
    private CoroutineScope p$;
    final /* synthetic */ CombineKt$zipImpl$$inlined$unsafeFlow$1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1(FlowCollector flowCollector, Continuation continuation, CombineKt$zipImpl$$inlined$unsafeFlow$1 combineKt$zipImpl$$inlined$unsafeFlow$1) {
        super(2, continuation);
        this.$this_unsafeFlow = flowCollector;
        this.this$0 = combineKt$zipImpl$$inlined$unsafeFlow$1;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = new CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1(this.$this_unsafeFlow, continuation, this.this$0);
        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.p$ = (CoroutineScope) obj;
        return combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v17, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v10, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v20, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v19, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v20, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v22, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x02a8, code lost:
        r0 = new kotlinx.coroutines.flow.internal.AbortFlowException(r12.$this_unsafeFlow);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x028e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x028f, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r10, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0293, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0294, code lost:
        r0 = e;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:77:0x026b, B:88:0x028d] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x02a8  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x02ba  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0195 A[Catch:{ all -> 0x0281 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01e1 A[Catch:{ all -> 0x0263 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01e4 A[Catch:{ all -> 0x0263 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01f0 A[Catch:{ all -> 0x0263 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x021f  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x024e  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0274  */
    public final Object invokeSuspend(Object obj) {
        ReceiveChannel receiveChannel;
        CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1;
        final ReceiveChannel receiveChannel2;
        Throwable th;
        CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12;
        ReceiveChannel receiveChannel3;
        ReceiveChannel receiveChannel4;
        ChannelIterator channelIterator;
        Throwable th2;
        ChannelIterator channelIterator2;
        CoroutineScope coroutineScope;
        ReceiveChannel receiveChannel5;
        ReceiveChannel receiveChannel6;
        Object obj2;
        ReceiveChannel receiveChannel7;
        ChannelIterator channelIterator3;
        Object obj3;
        ReceiveChannel receiveChannel8;
        ReceiveChannel receiveChannel9;
        ReceiveChannel receiveChannel10;
        CoroutineScope coroutineScope2;
        FlowCollector flowCollector;
        Object obj4;
        Object obj5;
        CoroutineScope coroutineScope3;
        ReceiveChannel receiveChannel11;
        ChannelIterator channelIterator4;
        ReceiveChannel receiveChannel12;
        ReceiveChannel receiveChannel13;
        CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13;
        ReceiveChannel receiveChannel14;
        ReceiveChannel receiveChannel15;
        ReceiveChannel receiveChannel16;
        Object obj6;
        Object obj7;
        Object next;
        ReceiveChannel receiveChannel17;
        Object obj8;
        ChannelIterator channelIterator5;
        ReceiveChannel receiveChannel18;
        CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$14;
        Throwable th3;
        ReceiveChannel receiveChannel19;
        ChannelIterator channelIterator6;
        CoroutineScope coroutineScope4;
        ReceiveChannel receiveChannel20;
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(obj);
            coroutineScope = this.p$;
            receiveChannel2 = CombineKt.asChannel(coroutineScope, this.this$0.$flow$inlined);
            receiveChannel = CombineKt.asChannel(coroutineScope, this.this$0.$flow2$inlined);
            if (receiveChannel != null) {
                ((SendChannel) receiveChannel).invokeOnClose(new Function1<Throwable, Unit>(this) {
                    final /* synthetic */ CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 this$0;

                    {
                        this.this$0 = r1;
                    }

                    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                        invoke((Throwable) obj);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(Throwable th) {
                        if (!receiveChannel2.isClosedForReceive()) {
                            receiveChannel2.cancel((CancellationException) new AbortFlowException(this.this$0.$this_unsafeFlow));
                        }
                    }
                });
                channelIterator2 = receiveChannel.iterator();
                try {
                    th2 = null;
                    try {
                        channelIterator = receiveChannel2.iterator();
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12 = this;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12;
                        receiveChannel4 = receiveChannel2;
                        receiveChannel3 = receiveChannel4;
                        receiveChannel6 = receiveChannel3;
                    } catch (Throwable th4) {
                        th = th4;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = this;
                        th = th;
                        throw th;
                    }
                } catch (AbortFlowException e) {
                    e = e;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = this;
                    try {
                        FlowExceptions_commonKt.checkOwnership(e, combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.$this_unsafeFlow);
                        if (!receiveChannel.isClosedForReceive()) {
                        }
                        return Unit.INSTANCE;
                    } catch (Throwable th5) {
                        th = th5;
                        if (!receiveChannel.isClosedForReceive()) {
                            receiveChannel.cancel(new AbortFlowException(combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.$this_unsafeFlow));
                        }
                        throw th;
                    }
                } catch (Throwable th6) {
                    th = th6;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = this;
                    if (!receiveChannel.isClosedForReceive()) {
                    }
                    throw th;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.channels.SendChannel<*>");
            }
        } else if (i == 1) {
            ChannelIterator channelIterator7 = (ChannelIterator) this.L$9;
            ReceiveChannel receiveChannel21 = (ReceiveChannel) this.L$8;
            Throwable th7 = (Throwable) this.L$7;
            receiveChannel2 = this.L$6;
            CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$15 = (CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1) this.L$5;
            ReceiveChannel receiveChannel22 = (ReceiveChannel) this.L$4;
            ChannelIterator channelIterator8 = (ChannelIterator) this.L$3;
            receiveChannel = this.L$2;
            ReceiveChannel receiveChannel23 = (ReceiveChannel) this.L$1;
            CoroutineScope coroutineScope5 = (CoroutineScope) this.L$0;
            ResultKt.throwOnFailure(obj);
            obj8 = obj;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = this;
            Throwable th8 = th7;
            channelIterator6 = channelIterator7;
            receiveChannel20 = receiveChannel23;
            receiveChannel18 = receiveChannel22;
            th3 = th8;
            CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$16 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$15;
            receiveChannel19 = receiveChannel21;
            coroutineScope4 = coroutineScope5;
            channelIterator5 = channelIterator8;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$14 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$16;
            try {
                if (!((Boolean) obj8).booleanValue()) {
                    obj2 = channelIterator6.next();
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$0 = coroutineScope4;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$1 = receiveChannel20;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$2 = receiveChannel;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$3 = channelIterator5;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$4 = receiveChannel18;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$5 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$14;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$6 = receiveChannel2;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$7 = th3;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$8 = receiveChannel19;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$9 = channelIterator6;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$10 = obj2;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$11 = obj2;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.label = 2;
                    obj6 = channelIterator5.hasNext(combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1);
                    if (obj6 == coroutine_suspended) {
                        return coroutine_suspended;
                    }
                    receiveChannel14 = receiveChannel;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1;
                    receiveChannel15 = receiveChannel20;
                    coroutineScope = coroutineScope4;
                    channelIterator2 = channelIterator5;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$14;
                    receiveChannel3 = receiveChannel2;
                    receiveChannel16 = receiveChannel19;
                    channelIterator = channelIterator6;
                    th2 = th3;
                    receiveChannel4 = receiveChannel18;
                    obj3 = obj2;
                    if (((Boolean) obj6).booleanValue()) {
                        FlowCollector flowCollector2 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.$this_unsafeFlow;
                        Function3 function3 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.this$0.$transform$inlined;
                        Object obj9 = coroutine_suspended;
                        if (obj2 != NullSurrogateKt.NULL) {
                        }
                        obj7 = NullSurrogateKt.NULL;
                        Function3 function32 = function3;
                        next = channelIterator2.next();
                        if (next == obj7) {
                        }
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$0 = coroutineScope;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$1 = receiveChannel15;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$2 = receiveChannel14;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$3 = channelIterator2;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$4 = receiveChannel4;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$5 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$6 = receiveChannel3;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$7 = th2;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$8 = receiveChannel16;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$9 = channelIterator;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$10 = obj3;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$11 = obj2;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$12 = flowCollector2;
                        combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.label = 3;
                        Object obj10 = r17;
                        CoroutineScope coroutineScope6 = coroutineScope;
                        obj5 = function32.invoke(obj10, next, combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13);
                        obj4 = obj9;
                        if (obj5 != obj4) {
                        }
                        return obj4;
                    }
                    receiveChannel6 = receiveChannel16;
                    receiveChannel2 = receiveChannel15;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13;
                    receiveChannel = receiveChannel14;
                    return coroutine_suspended;
                }
                Unit unit = Unit.INSTANCE;
                ChannelsKt.cancelConsumed(receiveChannel2, th3);
                if (!receiveChannel.isClosedForReceive()) {
                    AbortFlowException abortFlowException = new AbortFlowException(combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.$this_unsafeFlow);
                    receiveChannel.cancel((CancellationException) abortFlowException);
                }
                return Unit.INSTANCE;
            } catch (Throwable th9) {
                th = th9;
                th = th;
                throw th;
            }
        } else if (i == 2) {
            Object obj11 = this.L$11;
            Object obj12 = this.L$10;
            ChannelIterator channelIterator9 = (ChannelIterator) this.L$9;
            ReceiveChannel receiveChannel24 = (ReceiveChannel) this.L$8;
            Throwable th10 = (Throwable) this.L$7;
            receiveChannel2 = this.L$6;
            CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$17 = (CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1) this.L$5;
            ReceiveChannel receiveChannel25 = (ReceiveChannel) this.L$4;
            ChannelIterator channelIterator10 = (ChannelIterator) this.L$3;
            receiveChannel = this.L$2;
            receiveChannel15 = (ReceiveChannel) this.L$1;
            CoroutineScope coroutineScope7 = (CoroutineScope) this.L$0;
            ResultKt.throwOnFailure(obj);
            obj6 = obj;
            obj2 = obj11;
            coroutineScope = coroutineScope7;
            receiveChannel14 = receiveChannel;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13 = this;
            ReceiveChannel receiveChannel26 = receiveChannel25;
            obj3 = obj12;
            channelIterator2 = channelIterator10;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$17;
            receiveChannel3 = receiveChannel2;
            receiveChannel16 = receiveChannel24;
            channelIterator = channelIterator9;
            th2 = th10;
            receiveChannel4 = receiveChannel26;
            try {
                if (((Boolean) obj6).booleanValue()) {
                    receiveChannel6 = receiveChannel16;
                    receiveChannel2 = receiveChannel15;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13;
                    receiveChannel = receiveChannel14;
                }
                FlowCollector flowCollector22 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.$this_unsafeFlow;
                Function3 function33 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.this$0.$transform$inlined;
                Object obj92 = coroutine_suspended;
                Object obj13 = obj2 != NullSurrogateKt.NULL ? null : obj2;
                obj7 = NullSurrogateKt.NULL;
                Function3 function322 = function33;
                next = channelIterator2.next();
                if (next == obj7) {
                    next = null;
                }
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$0 = coroutineScope;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$1 = receiveChannel15;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$2 = receiveChannel14;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$3 = channelIterator2;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$4 = receiveChannel4;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$5 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$6 = receiveChannel3;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$7 = th2;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$8 = receiveChannel16;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$9 = channelIterator;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$10 = obj3;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$11 = obj2;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.L$12 = flowCollector22;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13.label = 3;
                Object obj102 = obj13;
                CoroutineScope coroutineScope62 = coroutineScope;
                obj5 = function322.invoke(obj102, next, combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13);
                obj4 = obj92;
                if (obj5 != obj4) {
                    return obj4;
                }
                flowCollector = flowCollector22;
                receiveChannel9 = receiveChannel15;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13;
                receiveChannel7 = receiveChannel4;
                receiveChannel10 = receiveChannel14;
                channelIterator3 = channelIterator2;
                coroutineScope2 = coroutineScope62;
                ReceiveChannel receiveChannel27 = receiveChannel16;
                receiveChannel17 = receiveChannel3;
                receiveChannel8 = receiveChannel27;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$0 = coroutineScope2;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$1 = receiveChannel9;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$2 = receiveChannel10;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$3 = channelIterator3;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$4 = receiveChannel7;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$5 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$6 = receiveChannel2;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$7 = th2;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$8 = receiveChannel8;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$9 = channelIterator;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$10 = obj3;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$11 = obj2;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.label = 4;
                if (flowCollector.emit(obj5, combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1) != obj4) {
                }
                return obj4;
                return obj4;
            } catch (Throwable th11) {
                th = th11;
                receiveChannel2 = receiveChannel3;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$13;
                receiveChannel = receiveChannel14;
                throw th;
            }
        } else if (i == 3) {
            flowCollector = (FlowCollector) this.L$12;
            Object obj14 = this.L$11;
            Object obj15 = this.L$10;
            ChannelIterator channelIterator11 = (ChannelIterator) this.L$9;
            ReceiveChannel receiveChannel28 = (ReceiveChannel) this.L$8;
            Throwable th12 = (Throwable) this.L$7;
            ReceiveChannel receiveChannel29 = (ReceiveChannel) this.L$6;
            CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$18 = (CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1) this.L$5;
            receiveChannel7 = (ReceiveChannel) this.L$4;
            ChannelIterator channelIterator12 = (ChannelIterator) this.L$3;
            ReceiveChannel receiveChannel30 = (ReceiveChannel) this.L$2;
            ReceiveChannel receiveChannel31 = (ReceiveChannel) this.L$1;
            CoroutineScope coroutineScope8 = (CoroutineScope) this.L$0;
            try {
                ResultKt.throwOnFailure(obj);
                receiveChannel9 = receiveChannel31;
                channelIterator = channelIterator11;
                obj3 = obj15;
                receiveChannel8 = receiveChannel28;
                receiveChannel17 = receiveChannel29;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = this;
                obj4 = coroutine_suspended;
                obj5 = obj;
                ChannelIterator channelIterator13 = channelIterator12;
                obj2 = obj14;
                coroutineScope2 = coroutineScope8;
                receiveChannel10 = receiveChannel30;
                th2 = th12;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$18;
                channelIterator3 = channelIterator13;
                try {
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$0 = coroutineScope2;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$1 = receiveChannel9;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$2 = receiveChannel10;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$3 = channelIterator3;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$4 = receiveChannel7;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$5 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$6 = receiveChannel2;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$7 = th2;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$8 = receiveChannel8;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$9 = channelIterator;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$10 = obj3;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$11 = obj2;
                    combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.label = 4;
                    if (flowCollector.emit(obj5, combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1) != obj4) {
                        return obj4;
                    }
                    coroutine_suspended = obj4;
                    coroutineScope3 = coroutineScope2;
                    receiveChannel5 = receiveChannel8;
                    receiveChannel11 = receiveChannel2;
                    channelIterator4 = channelIterator3;
                    receiveChannel12 = receiveChannel9;
                    ReceiveChannel receiveChannel32 = receiveChannel7;
                    receiveChannel = receiveChannel10;
                    receiveChannel13 = receiveChannel32;
                    receiveChannel6 = receiveChannel5;
                    return obj4;
                } catch (Throwable th13) {
                    th = th13;
                    receiveChannel = receiveChannel10;
                    throw th;
                }
            } catch (Throwable th14) {
                th = th14;
                receiveChannel = receiveChannel30;
                receiveChannel2 = receiveChannel29;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = this;
                th = th;
                throw th;
            }
        } else if (i == 4) {
            ChannelIterator channelIterator14 = (ChannelIterator) this.L$9;
            ReceiveChannel receiveChannel33 = (ReceiveChannel) this.L$8;
            Throwable th15 = (Throwable) this.L$7;
            receiveChannel2 = (ReceiveChannel) this.L$6;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12 = (CombineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1) this.L$5;
            ReceiveChannel receiveChannel34 = (ReceiveChannel) this.L$4;
            ChannelIterator channelIterator15 = (ChannelIterator) this.L$3;
            receiveChannel = (ReceiveChannel) this.L$2;
            ReceiveChannel receiveChannel35 = (ReceiveChannel) this.L$1;
            CoroutineScope coroutineScope9 = (CoroutineScope) this.L$0;
            try {
                ResultKt.throwOnFailure(obj);
                channelIterator = channelIterator14;
                coroutineScope3 = coroutineScope9;
                th2 = th15;
                receiveChannel13 = receiveChannel34;
                channelIterator4 = channelIterator15;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = this;
                receiveChannel5 = receiveChannel33;
                receiveChannel11 = receiveChannel2;
                receiveChannel12 = receiveChannel35;
                receiveChannel6 = receiveChannel5;
            } catch (Throwable th16) {
                th = th16;
                combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1 = this;
                th = th;
                throw th;
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$0 = coroutineScope;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$1 = receiveChannel2;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$2 = receiveChannel;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$3 = channelIterator2;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$4 = receiveChannel4;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$5 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$6 = receiveChannel3;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$7 = th2;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$8 = receiveChannel6;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.L$9 = channelIterator;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$1.label = 1;
            obj8 = channelIterator.hasNext(combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12);
            if (obj8 == coroutine_suspended) {
                return coroutine_suspended;
            }
            ChannelIterator channelIterator16 = channelIterator2;
            coroutineScope4 = coroutineScope;
            receiveChannel20 = receiveChannel2;
            receiveChannel2 = receiveChannel3;
            combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$14 = combineKt$zipImpl$$inlined$unsafeFlow$1$lambda$12;
            channelIterator5 = channelIterator16;
            ReceiveChannel receiveChannel36 = receiveChannel4;
            th3 = th2;
            channelIterator6 = channelIterator;
            receiveChannel19 = receiveChannel6;
            receiveChannel18 = receiveChannel36;
            if (!((Boolean) obj8).booleanValue()) {
            }
            Unit unit2 = Unit.INSTANCE;
            ChannelsKt.cancelConsumed(receiveChannel2, th3);
            if (!receiveChannel.isClosedForReceive()) {
            }
            return Unit.INSTANCE;
            return coroutine_suspended;
        } catch (Throwable th17) {
            th = th17;
            receiveChannel2 = receiveChannel3;
            throw th;
        }
    }
}
