package kotlinx.coroutines.channels;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlin.ranges.RangesKt;
import kotlinx.coroutines.DelayKt;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.EventLoop_commonKt;
import kotlinx.coroutines.GlobalScope;
import kotlinx.coroutines.TimeSource;
import kotlinx.coroutines.TimeSourceKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a/\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00010\u0006H@ø\u0001\u0000¢\u0006\u0002\u0010\u0007\u001a/\u0010\b\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00010\u0006H@ø\u0001\u0000¢\u0006\u0002\u0010\u0007\u001a4\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00010\n2\u0006\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\u000eH\u0007\u0002\u0004\n\u0002\b\u0019¨\u0006\u000f"}, d2 = {"fixedDelayTicker", "", "delayMillis", "", "initialDelayMillis", "channel", "Lkotlinx/coroutines/channels/SendChannel;", "(JJLkotlinx/coroutines/channels/SendChannel;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fixedPeriodTicker", "ticker", "Lkotlinx/coroutines/channels/ReceiveChannel;", "context", "Lkotlin/coroutines/CoroutineContext;", "mode", "Lkotlinx/coroutines/channels/TickerMode;", "kotlinx-coroutines-core"}, k = 2, mv = {1, 1, 16})
/* compiled from: TickerChannels.kt */
public final class TickerChannelsKt {

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[TickerMode.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[TickerMode.FIXED_PERIOD.ordinal()] = 1;
            $EnumSwitchMapping$0[TickerMode.FIXED_DELAY.ordinal()] = 2;
        }
    }

    public static /* synthetic */ ReceiveChannel ticker$default(long j, long j2, CoroutineContext coroutineContext, TickerMode tickerMode, int i, Object obj) {
        if ((i & 2) != 0) {
            j2 = j;
        }
        if ((i & 4) != 0) {
            coroutineContext = EmptyCoroutineContext.INSTANCE;
        }
        if ((i & 8) != 0) {
            tickerMode = TickerMode.FIXED_PERIOD;
        }
        return ticker(j, j2, coroutineContext, tickerMode);
    }

    public static final ReceiveChannel<Unit> ticker(long j, long j2, CoroutineContext coroutineContext, TickerMode tickerMode) {
        long j3 = j2;
        boolean z = true;
        if (j >= 0) {
            if (j3 < 0) {
                z = false;
            }
            if (z) {
                CoroutineContext coroutineContext2 = coroutineContext;
                return ProduceKt.produce(GlobalScope.INSTANCE, Dispatchers.getUnconfined().plus(coroutineContext), 0, new TickerChannelsKt$ticker$3(tickerMode, j, j2, (Continuation) null));
            }
            throw new IllegalArgumentException(("Expected non-negative initial delay, but has " + j2 + " ms").toString());
        }
        throw new IllegalArgumentException(("Expected non-negative delay, but has " + j + " ms").toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ed, code lost:
        r2 = kotlin.coroutines.jvm.internal.Boxing.boxLong(r2.nanoTime());
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00df A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0174 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    static final /* synthetic */ Object fixedPeriodTicker(long j, long j2, SendChannel<? super Unit> sendChannel, Continuation<? super Unit> continuation) {
        TickerChannelsKt$fixedPeriodTicker$1 tickerChannelsKt$fixedPeriodTicker$1;
        Object coroutine_suspended;
        int i;
        long j3;
        long j4;
        long j5;
        SendChannel<? super Unit> sendChannel2;
        char c;
        long j6;
        long j7;
        long j8;
        SendChannel<? super Unit> sendChannel3;
        long j9;
        long j10;
        long coerceAtLeast;
        TickerChannelsKt$fixedPeriodTicker$1 tickerChannelsKt$fixedPeriodTicker$12;
        long delayNanosToMillis;
        Long boxLong;
        Unit unit;
        Long boxLong2;
        long j11 = j2;
        Continuation<? super Unit> continuation2 = continuation;
        if (continuation2 instanceof TickerChannelsKt$fixedPeriodTicker$1) {
            tickerChannelsKt$fixedPeriodTicker$1 = (TickerChannelsKt$fixedPeriodTicker$1) continuation2;
            if ((tickerChannelsKt$fixedPeriodTicker$1.label & Integer.MIN_VALUE) != 0) {
                tickerChannelsKt$fixedPeriodTicker$1.label -= Integer.MIN_VALUE;
                Object obj = tickerChannelsKt$fixedPeriodTicker$1.result;
                coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
                i = tickerChannelsKt$fixedPeriodTicker$1.label;
                int i2 = 2;
                if (i != 0) {
                    ResultKt.throwOnFailure(obj);
                    TimeSource timeSource = TimeSourceKt.getTimeSource();
                    long nanoTime = ((timeSource == null || (boxLong2 = Boxing.boxLong(timeSource.nanoTime())) == null) ? System.nanoTime() : boxLong2.longValue()) + EventLoop_commonKt.delayToNanos(j2);
                    long j12 = j;
                    tickerChannelsKt$fixedPeriodTicker$1.J$0 = j12;
                    tickerChannelsKt$fixedPeriodTicker$1.J$1 = j11;
                    sendChannel3 = sendChannel;
                    tickerChannelsKt$fixedPeriodTicker$1.L$0 = sendChannel3;
                    tickerChannelsKt$fixedPeriodTicker$1.J$2 = nanoTime;
                    tickerChannelsKt$fixedPeriodTicker$1.label = 1;
                    if (DelayKt.delay(j11, tickerChannelsKt$fixedPeriodTicker$1) == coroutine_suspended) {
                        return coroutine_suspended;
                    }
                    j5 = nanoTime;
                    j7 = j12;
                } else if (i == 1) {
                    long j13 = tickerChannelsKt$fixedPeriodTicker$1.J$2;
                    long j14 = tickerChannelsKt$fixedPeriodTicker$1.J$1;
                    j7 = tickerChannelsKt$fixedPeriodTicker$1.J$0;
                    ResultKt.throwOnFailure(obj);
                    sendChannel3 = (SendChannel) tickerChannelsKt$fixedPeriodTicker$1.L$0;
                    long j15 = j13;
                    j11 = j14;
                    j5 = j15;
                } else if (i == 2) {
                    j6 = tickerChannelsKt$fixedPeriodTicker$1.J$3;
                    j10 = tickerChannelsKt$fixedPeriodTicker$1.J$2;
                    sendChannel2 = (SendChannel) tickerChannelsKt$fixedPeriodTicker$1.L$0;
                    j4 = tickerChannelsKt$fixedPeriodTicker$1.J$1;
                    j3 = tickerChannelsKt$fixedPeriodTicker$1.J$0;
                    ResultKt.throwOnFailure(obj);
                    TimeSource timeSource2 = TimeSourceKt.getTimeSource();
                    if (timeSource2 == null || boxLong == null) {
                    }
                    long j16 = r15;
                    TickerChannelsKt$fixedPeriodTicker$1 tickerChannelsKt$fixedPeriodTicker$13 = tickerChannelsKt$fixedPeriodTicker$1;
                    long j17 = j10;
                    coerceAtLeast = RangesKt.coerceAtLeast(j10 - j16, 0);
                    if (coerceAtLeast != 0 || j6 == 0) {
                    }
                    tickerChannelsKt$fixedPeriodTicker$12 = tickerChannelsKt$fixedPeriodTicker$13;
                    long j18 = coerceAtLeast;
                    long j19 = j16;
                    long j20 = j18;
                    delayNanosToMillis = EventLoop_commonKt.delayNanosToMillis(j20);
                    tickerChannelsKt$fixedPeriodTicker$12.J$0 = j3;
                    tickerChannelsKt$fixedPeriodTicker$12.J$1 = j4;
                    tickerChannelsKt$fixedPeriodTicker$12.L$0 = sendChannel2;
                    long j21 = j4;
                    long j22 = j17;
                    tickerChannelsKt$fixedPeriodTicker$12.J$2 = j22;
                    tickerChannelsKt$fixedPeriodTicker$12.J$3 = j6;
                    tickerChannelsKt$fixedPeriodTicker$12.J$4 = j19;
                    tickerChannelsKt$fixedPeriodTicker$12.J$5 = j20;
                    c = 4;
                    tickerChannelsKt$fixedPeriodTicker$12.label = 4;
                    if (DelayKt.delay(delayNanosToMillis, tickerChannelsKt$fixedPeriodTicker$12) == coroutine_suspended) {
                    }
                    return coroutine_suspended;
                } else if (i == 3) {
                    long j23 = tickerChannelsKt$fixedPeriodTicker$1.J$6;
                    long j24 = tickerChannelsKt$fixedPeriodTicker$1.J$5;
                    long j25 = tickerChannelsKt$fixedPeriodTicker$1.J$4;
                    j6 = tickerChannelsKt$fixedPeriodTicker$1.J$3;
                    j9 = tickerChannelsKt$fixedPeriodTicker$1.J$2;
                    sendChannel2 = (SendChannel) tickerChannelsKt$fixedPeriodTicker$1.L$0;
                    j4 = tickerChannelsKt$fixedPeriodTicker$1.J$1;
                    j3 = tickerChannelsKt$fixedPeriodTicker$1.J$0;
                    ResultKt.throwOnFailure(obj);
                    c = 4;
                    long j26 = j4;
                    j7 = j3;
                    j8 = j6;
                    j11 = j26;
                    char c2 = c;
                    sendChannel3 = sendChannel2;
                    i2 = 2;
                    j10 = j5 + j8;
                    unit = Unit.INSTANCE;
                    tickerChannelsKt$fixedPeriodTicker$1.J$0 = j7;
                    tickerChannelsKt$fixedPeriodTicker$1.J$1 = j11;
                    tickerChannelsKt$fixedPeriodTicker$1.L$0 = sendChannel3;
                    tickerChannelsKt$fixedPeriodTicker$1.J$2 = j10;
                    tickerChannelsKt$fixedPeriodTicker$1.J$3 = j8;
                    tickerChannelsKt$fixedPeriodTicker$1.label = i2;
                    if (sendChannel3.send(unit, tickerChannelsKt$fixedPeriodTicker$1) != coroutine_suspended) {
                    }
                    return coroutine_suspended;
                } else if (i == 4) {
                    long j27 = tickerChannelsKt$fixedPeriodTicker$1.J$5;
                    long j28 = tickerChannelsKt$fixedPeriodTicker$1.J$4;
                    j6 = tickerChannelsKt$fixedPeriodTicker$1.J$3;
                    j9 = tickerChannelsKt$fixedPeriodTicker$1.J$2;
                    sendChannel2 = (SendChannel) tickerChannelsKt$fixedPeriodTicker$1.L$0;
                    j4 = tickerChannelsKt$fixedPeriodTicker$1.J$1;
                    j3 = tickerChannelsKt$fixedPeriodTicker$1.J$0;
                    ResultKt.throwOnFailure(obj);
                    c = 4;
                    long j262 = j4;
                    j7 = j3;
                    j8 = j6;
                    j11 = j262;
                    char c22 = c;
                    sendChannel3 = sendChannel2;
                    i2 = 2;
                    j10 = j5 + j8;
                    unit = Unit.INSTANCE;
                    tickerChannelsKt$fixedPeriodTicker$1.J$0 = j7;
                    tickerChannelsKt$fixedPeriodTicker$1.J$1 = j11;
                    tickerChannelsKt$fixedPeriodTicker$1.L$0 = sendChannel3;
                    tickerChannelsKt$fixedPeriodTicker$1.J$2 = j10;
                    tickerChannelsKt$fixedPeriodTicker$1.J$3 = j8;
                    tickerChannelsKt$fixedPeriodTicker$1.label = i2;
                    if (sendChannel3.send(unit, tickerChannelsKt$fixedPeriodTicker$1) != coroutine_suspended) {
                        return coroutine_suspended;
                    }
                    sendChannel2 = sendChannel3;
                    long j29 = j11;
                    j6 = j8;
                    j3 = j7;
                    j4 = j29;
                    TimeSource timeSource22 = TimeSourceKt.getTimeSource();
                    long nanoTime2 = (timeSource22 == null || boxLong == null) ? System.nanoTime() : boxLong.longValue();
                    long j162 = nanoTime2;
                    TickerChannelsKt$fixedPeriodTicker$1 tickerChannelsKt$fixedPeriodTicker$132 = tickerChannelsKt$fixedPeriodTicker$1;
                    long j172 = j10;
                    coerceAtLeast = RangesKt.coerceAtLeast(j10 - j162, 0);
                    if (coerceAtLeast != 0 || j6 == 0) {
                        tickerChannelsKt$fixedPeriodTicker$12 = tickerChannelsKt$fixedPeriodTicker$132;
                        long j182 = coerceAtLeast;
                        long j192 = j162;
                        long j202 = j182;
                        delayNanosToMillis = EventLoop_commonKt.delayNanosToMillis(j202);
                        tickerChannelsKt$fixedPeriodTicker$12.J$0 = j3;
                        tickerChannelsKt$fixedPeriodTicker$12.J$1 = j4;
                        tickerChannelsKt$fixedPeriodTicker$12.L$0 = sendChannel2;
                        long j212 = j4;
                        long j222 = j172;
                        tickerChannelsKt$fixedPeriodTicker$12.J$2 = j222;
                        tickerChannelsKt$fixedPeriodTicker$12.J$3 = j6;
                        tickerChannelsKt$fixedPeriodTicker$12.J$4 = j192;
                        tickerChannelsKt$fixedPeriodTicker$12.J$5 = j202;
                        c = 4;
                        tickerChannelsKt$fixedPeriodTicker$12.label = 4;
                        if (DelayKt.delay(delayNanosToMillis, tickerChannelsKt$fixedPeriodTicker$12) == coroutine_suspended) {
                            return coroutine_suspended;
                        }
                        tickerChannelsKt$fixedPeriodTicker$1 = tickerChannelsKt$fixedPeriodTicker$12;
                        j9 = j222;
                        j4 = j212;
                        long j2622 = j4;
                        j7 = j3;
                        j8 = j6;
                        j11 = j2622;
                        char c222 = c;
                        sendChannel3 = sendChannel2;
                        i2 = 2;
                        j10 = j5 + j8;
                        unit = Unit.INSTANCE;
                        tickerChannelsKt$fixedPeriodTicker$1.J$0 = j7;
                        tickerChannelsKt$fixedPeriodTicker$1.J$1 = j11;
                        tickerChannelsKt$fixedPeriodTicker$1.L$0 = sendChannel3;
                        tickerChannelsKt$fixedPeriodTicker$1.J$2 = j10;
                        tickerChannelsKt$fixedPeriodTicker$1.J$3 = j8;
                        tickerChannelsKt$fixedPeriodTicker$1.label = i2;
                        if (sendChannel3.send(unit, tickerChannelsKt$fixedPeriodTicker$1) != coroutine_suspended) {
                        }
                        return coroutine_suspended;
                    }
                    long j30 = j6 - ((j162 - j172) % j6);
                    long j31 = coerceAtLeast;
                    long j32 = j162 + j30;
                    long j33 = j162;
                    long delayNanosToMillis2 = EventLoop_commonKt.delayNanosToMillis(j30);
                    TickerChannelsKt$fixedPeriodTicker$1 tickerChannelsKt$fixedPeriodTicker$14 = tickerChannelsKt$fixedPeriodTicker$132;
                    tickerChannelsKt$fixedPeriodTicker$14.J$0 = j3;
                    tickerChannelsKt$fixedPeriodTicker$14.J$1 = j4;
                    tickerChannelsKt$fixedPeriodTicker$14.L$0 = sendChannel2;
                    tickerChannelsKt$fixedPeriodTicker$14.J$2 = j32;
                    tickerChannelsKt$fixedPeriodTicker$14.J$3 = j6;
                    long j34 = j32;
                    tickerChannelsKt$fixedPeriodTicker$14.J$4 = j33;
                    tickerChannelsKt$fixedPeriodTicker$14.J$5 = j31;
                    tickerChannelsKt$fixedPeriodTicker$14.J$6 = j30;
                    tickerChannelsKt$fixedPeriodTicker$14.label = 3;
                    if (DelayKt.delay(delayNanosToMillis2, tickerChannelsKt$fixedPeriodTicker$14) == coroutine_suspended) {
                        return coroutine_suspended;
                    }
                    tickerChannelsKt$fixedPeriodTicker$1 = tickerChannelsKt$fixedPeriodTicker$14;
                    j9 = j34;
                    c = 4;
                    long j26222 = j4;
                    j7 = j3;
                    j8 = j6;
                    j11 = j26222;
                    char c2222 = c;
                    sendChannel3 = sendChannel2;
                    i2 = 2;
                    j10 = j5 + j8;
                    unit = Unit.INSTANCE;
                    tickerChannelsKt$fixedPeriodTicker$1.J$0 = j7;
                    tickerChannelsKt$fixedPeriodTicker$1.J$1 = j11;
                    tickerChannelsKt$fixedPeriodTicker$1.L$0 = sendChannel3;
                    tickerChannelsKt$fixedPeriodTicker$1.J$2 = j10;
                    tickerChannelsKt$fixedPeriodTicker$1.J$3 = j8;
                    tickerChannelsKt$fixedPeriodTicker$1.label = i2;
                    if (sendChannel3.send(unit, tickerChannelsKt$fixedPeriodTicker$1) != coroutine_suspended) {
                    }
                    return coroutine_suspended;
                    tickerChannelsKt$fixedPeriodTicker$12 = tickerChannelsKt$fixedPeriodTicker$132;
                    long j1822 = coerceAtLeast;
                    long j1922 = j162;
                    long j2022 = j1822;
                    delayNanosToMillis = EventLoop_commonKt.delayNanosToMillis(j2022);
                    tickerChannelsKt$fixedPeriodTicker$12.J$0 = j3;
                    tickerChannelsKt$fixedPeriodTicker$12.J$1 = j4;
                    tickerChannelsKt$fixedPeriodTicker$12.L$0 = sendChannel2;
                    long j2122 = j4;
                    long j2222 = j172;
                    tickerChannelsKt$fixedPeriodTicker$12.J$2 = j2222;
                    tickerChannelsKt$fixedPeriodTicker$12.J$3 = j6;
                    tickerChannelsKt$fixedPeriodTicker$12.J$4 = j1922;
                    tickerChannelsKt$fixedPeriodTicker$12.J$5 = j2022;
                    c = 4;
                    tickerChannelsKt$fixedPeriodTicker$12.label = 4;
                    if (DelayKt.delay(delayNanosToMillis, tickerChannelsKt$fixedPeriodTicker$12) == coroutine_suspended) {
                    }
                    return coroutine_suspended;
                    return coroutine_suspended;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                j8 = EventLoop_commonKt.delayToNanos(j7);
                j10 = j5 + j8;
                unit = Unit.INSTANCE;
                tickerChannelsKt$fixedPeriodTicker$1.J$0 = j7;
                tickerChannelsKt$fixedPeriodTicker$1.J$1 = j11;
                tickerChannelsKt$fixedPeriodTicker$1.L$0 = sendChannel3;
                tickerChannelsKt$fixedPeriodTicker$1.J$2 = j10;
                tickerChannelsKt$fixedPeriodTicker$1.J$3 = j8;
                tickerChannelsKt$fixedPeriodTicker$1.label = i2;
                if (sendChannel3.send(unit, tickerChannelsKt$fixedPeriodTicker$1) != coroutine_suspended) {
                }
                return coroutine_suspended;
            }
        }
        tickerChannelsKt$fixedPeriodTicker$1 = new TickerChannelsKt$fixedPeriodTicker$1(continuation2);
        Object obj2 = tickerChannelsKt$fixedPeriodTicker$1.result;
        coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        i = tickerChannelsKt$fixedPeriodTicker$1.label;
        int i22 = 2;
        if (i != 0) {
        }
        j8 = EventLoop_commonKt.delayToNanos(j7);
        j10 = j5 + j8;
        unit = Unit.INSTANCE;
        tickerChannelsKt$fixedPeriodTicker$1.J$0 = j7;
        tickerChannelsKt$fixedPeriodTicker$1.J$1 = j11;
        tickerChannelsKt$fixedPeriodTicker$1.L$0 = sendChannel3;
        tickerChannelsKt$fixedPeriodTicker$1.J$2 = j10;
        tickerChannelsKt$fixedPeriodTicker$1.J$3 = j8;
        tickerChannelsKt$fixedPeriodTicker$1.label = i22;
        if (sendChannel3.send(unit, tickerChannelsKt$fixedPeriodTicker$1) != coroutine_suspended) {
        }
        return coroutine_suspended;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v3, resolved type: kotlinx.coroutines.channels.SendChannel<? super kotlin.Unit>} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007f A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0092 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    static final /* synthetic */ Object fixedDelayTicker(long j, long j2, SendChannel<? super Unit> sendChannel, Continuation<? super Unit> continuation) {
        TickerChannelsKt$fixedDelayTicker$1 tickerChannelsKt$fixedDelayTicker$1;
        Object coroutine_suspended;
        int i;
        long j3;
        long j4;
        SendChannel<? super Unit> sendChannel2;
        Unit unit;
        if (continuation instanceof TickerChannelsKt$fixedDelayTicker$1) {
            tickerChannelsKt$fixedDelayTicker$1 = (TickerChannelsKt$fixedDelayTicker$1) continuation;
            if ((tickerChannelsKt$fixedDelayTicker$1.label & Integer.MIN_VALUE) != 0) {
                tickerChannelsKt$fixedDelayTicker$1.label -= Integer.MIN_VALUE;
                Object obj = tickerChannelsKt$fixedDelayTicker$1.result;
                coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
                i = tickerChannelsKt$fixedDelayTicker$1.label;
                if (i != 0) {
                    ResultKt.throwOnFailure(obj);
                    tickerChannelsKt$fixedDelayTicker$1.J$0 = j;
                    tickerChannelsKt$fixedDelayTicker$1.J$1 = j2;
                    tickerChannelsKt$fixedDelayTicker$1.L$0 = sendChannel;
                    tickerChannelsKt$fixedDelayTicker$1.label = 1;
                    if (DelayKt.delay(j2, tickerChannelsKt$fixedDelayTicker$1) == coroutine_suspended) {
                        return coroutine_suspended;
                    }
                    unit = Unit.INSTANCE;
                    tickerChannelsKt$fixedDelayTicker$1.J$0 = j;
                    tickerChannelsKt$fixedDelayTicker$1.J$1 = j2;
                    tickerChannelsKt$fixedDelayTicker$1.L$0 = sendChannel;
                    tickerChannelsKt$fixedDelayTicker$1.label = 2;
                    if (sendChannel.send(unit, tickerChannelsKt$fixedDelayTicker$1) != coroutine_suspended) {
                    }
                    return coroutine_suspended;
                } else if (i == 1) {
                    sendChannel = tickerChannelsKt$fixedDelayTicker$1.L$0;
                    j2 = tickerChannelsKt$fixedDelayTicker$1.J$1;
                    j = tickerChannelsKt$fixedDelayTicker$1.J$0;
                    ResultKt.throwOnFailure(obj);
                    unit = Unit.INSTANCE;
                    tickerChannelsKt$fixedDelayTicker$1.J$0 = j;
                    tickerChannelsKt$fixedDelayTicker$1.J$1 = j2;
                    tickerChannelsKt$fixedDelayTicker$1.L$0 = sendChannel;
                    tickerChannelsKt$fixedDelayTicker$1.label = 2;
                    if (sendChannel.send(unit, tickerChannelsKt$fixedDelayTicker$1) != coroutine_suspended) {
                        return coroutine_suspended;
                    }
                    long j5 = j;
                    sendChannel2 = sendChannel;
                    j4 = j2;
                    j3 = j5;
                    tickerChannelsKt$fixedDelayTicker$1.J$0 = j3;
                    tickerChannelsKt$fixedDelayTicker$1.J$1 = j4;
                    tickerChannelsKt$fixedDelayTicker$1.L$0 = sendChannel2;
                    tickerChannelsKt$fixedDelayTicker$1.label = 3;
                    if (DelayKt.delay(j3, tickerChannelsKt$fixedDelayTicker$1) == coroutine_suspended) {
                    }
                    return coroutine_suspended;
                } else if (i == 2) {
                    sendChannel2 = (SendChannel) tickerChannelsKt$fixedDelayTicker$1.L$0;
                    j4 = tickerChannelsKt$fixedDelayTicker$1.J$1;
                    j3 = tickerChannelsKt$fixedDelayTicker$1.J$0;
                    ResultKt.throwOnFailure(obj);
                    tickerChannelsKt$fixedDelayTicker$1.J$0 = j3;
                    tickerChannelsKt$fixedDelayTicker$1.J$1 = j4;
                    tickerChannelsKt$fixedDelayTicker$1.L$0 = sendChannel2;
                    tickerChannelsKt$fixedDelayTicker$1.label = 3;
                    if (DelayKt.delay(j3, tickerChannelsKt$fixedDelayTicker$1) == coroutine_suspended) {
                        return coroutine_suspended;
                    }
                } else if (i == 3) {
                    sendChannel2 = (SendChannel) tickerChannelsKt$fixedDelayTicker$1.L$0;
                    j4 = tickerChannelsKt$fixedDelayTicker$1.J$1;
                    j3 = tickerChannelsKt$fixedDelayTicker$1.J$0;
                    ResultKt.throwOnFailure(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                sendChannel = sendChannel2;
                j2 = j4;
                j = j3;
                unit = Unit.INSTANCE;
                tickerChannelsKt$fixedDelayTicker$1.J$0 = j;
                tickerChannelsKt$fixedDelayTicker$1.J$1 = j2;
                tickerChannelsKt$fixedDelayTicker$1.L$0 = sendChannel;
                tickerChannelsKt$fixedDelayTicker$1.label = 2;
                if (sendChannel.send(unit, tickerChannelsKt$fixedDelayTicker$1) != coroutine_suspended) {
                }
                return coroutine_suspended;
            }
        }
        tickerChannelsKt$fixedDelayTicker$1 = new TickerChannelsKt$fixedDelayTicker$1(continuation);
        Object obj2 = tickerChannelsKt$fixedDelayTicker$1.result;
        coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        i = tickerChannelsKt$fixedDelayTicker$1.label;
        if (i != 0) {
        }
        sendChannel = sendChannel2;
        j2 = j4;
        j = j3;
        unit = Unit.INSTANCE;
        tickerChannelsKt$fixedDelayTicker$1.J$0 = j;
        tickerChannelsKt$fixedDelayTicker$1.J$1 = j2;
        tickerChannelsKt$fixedDelayTicker$1.L$0 = sendChannel;
        tickerChannelsKt$fixedDelayTicker$1.label = 2;
        if (sendChannel.send(unit, tickerChannelsKt$fixedDelayTicker$1) != coroutine_suspended) {
        }
        return coroutine_suspended;
    }
}
