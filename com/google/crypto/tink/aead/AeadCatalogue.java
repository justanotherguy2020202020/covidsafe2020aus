package com.google.crypto.tink.aead;

import com.google.crypto.tink.Aead;
import com.google.crypto.tink.Catalogue;
import com.google.crypto.tink.KeyManager;
import java.security.GeneralSecurityException;

class AeadCatalogue implements Catalogue<Aead> {
    public KeyManager<Aead> getKeyManager(String str, String str2, int i) throws GeneralSecurityException {
        String lowerCase = str2.toLowerCase();
        if (((lowerCase.hashCode() == 2989895 && lowerCase.equals("aead")) ? (char) 0 : 65535) == 0) {
            KeyManager<Aead> aeadKeyManager = aeadKeyManager(str);
            if (aeadKeyManager.getVersion() >= i) {
                return aeadKeyManager;
            }
            throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", new Object[]{str, Integer.valueOf(i)}));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", new Object[]{str2}));
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    private KeyManager<Aead> aeadKeyManager(String str) throws GeneralSecurityException {
        char c;
        switch (str.hashCode()) {
            case 360753376:
                if (str.equals("type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key")) {
                    c = 3;
                    break;
                }
            case 1215885937:
                if (str.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey")) {
                    c = 0;
                    break;
                }
            case 1469984853:
                if (str.equals("type.googleapis.com/google.crypto.tink.KmsAeadKey")) {
                    c = 4;
                    break;
                }
            case 1797113348:
                if (str.equals("type.googleapis.com/google.crypto.tink.AesEaxKey")) {
                    c = 1;
                    break;
                }
            case 1855890991:
                if (str.equals("type.googleapis.com/google.crypto.tink.AesGcmKey")) {
                    c = 2;
                    break;
                }
            case 2079211877:
                if (str.equals("type.googleapis.com/google.crypto.tink.KmsEnvelopeAeadKey")) {
                    c = 5;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        if (c == 0) {
            return new AesCtrHmacAeadKeyManager();
        }
        if (c == 1) {
            return new AesEaxKeyManager();
        }
        if (c == 2) {
            return new AesGcmKeyManager();
        }
        if (c == 3) {
            return new ChaCha20Poly1305KeyManager();
        }
        if (c == 4) {
            return new KmsAeadKeyManager();
        }
        if (c == 5) {
            return new KmsEnvelopeAeadKeyManager();
        }
        throw new GeneralSecurityException(String.format("No support for primitive 'Aead' with key type '%s'.", new Object[]{str}));
    }
}
