package com.google.crypto.tink.streamingaead;

import com.google.crypto.tink.PrimitiveSet;
import com.google.crypto.tink.StreamingAead;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;

final class InputStreamDecrypter extends InputStream {
    byte[] associatedData;
    boolean attemptedMatching = false;
    InputStream ciphertextStream;
    InputStream matchingStream = null;
    PrimitiveSet<StreamingAead> primitives;

    public boolean markSupported() {
        return false;
    }

    public InputStreamDecrypter(PrimitiveSet<StreamingAead> primitiveSet, InputStream inputStream, byte[] bArr) {
        this.primitives = primitiveSet;
        if (inputStream.markSupported()) {
            this.ciphertextStream = inputStream;
        } else {
            this.ciphertextStream = new BufferedInputStream(inputStream);
        }
        this.ciphertextStream.mark(Integer.MAX_VALUE);
        this.associatedData = (byte[]) bArr.clone();
    }

    private void rewind() throws IOException {
        this.ciphertextStream.reset();
    }

    private void disableRewinding() throws IOException {
        this.ciphertextStream.mark(0);
    }

    public synchronized int available() throws IOException {
        if (this.matchingStream == null) {
            return 0;
        }
        return this.matchingStream.available();
    }

    public synchronized int read() throws IOException {
        byte[] bArr = new byte[1];
        if (read(bArr) != 1) {
            return -1;
        }
        return bArr[0];
    }

    public synchronized int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x004f, code lost:
        return r3;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0050 */
    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        if (i2 == 0) {
            return 0;
        }
        if (this.matchingStream != null) {
            return this.matchingStream.read(bArr, i, i2);
        } else if (!this.attemptedMatching) {
            this.attemptedMatching = true;
            try {
                for (PrimitiveSet.Entry primitive : this.primitives.getRawPrimitives()) {
                    InputStream newDecryptingStream = ((StreamingAead) primitive.getPrimitive()).newDecryptingStream(this.ciphertextStream, this.associatedData);
                    int read = newDecryptingStream.read(bArr, i, i2);
                    if (read == 0) {
                        rewind();
                        this.attemptedMatching = false;
                    } else {
                        this.matchingStream = newDecryptingStream;
                        disableRewinding();
                    }
                }
                throw new IOException("No matching key found for the ciphertext in the stream.");
            } catch (GeneralSecurityException e) {
                throw new IOException("Keyset failure: ", e);
            } catch (IOException unused) {
                rewind();
            } catch (GeneralSecurityException ) {
                rewind();
            }
        } else {
            throw new IOException("No matching key found for the ciphertext in the stream.");
        }
    }

    public synchronized void close() throws IOException {
        this.ciphertextStream.close();
    }
}
