package com.google.crypto.tink.streamingaead;

import com.google.crypto.tink.Catalogue;
import com.google.crypto.tink.KeyManager;
import com.google.crypto.tink.StreamingAead;
import java.security.GeneralSecurityException;

class StreamingAeadCatalogue implements Catalogue<StreamingAead> {
    public KeyManager<StreamingAead> getKeyManager(String str, String str2, int i) throws GeneralSecurityException {
        String lowerCase = str2.toLowerCase();
        if (((lowerCase.hashCode() == 754366121 && lowerCase.equals("streamingaead")) ? (char) 0 : 65535) == 0) {
            KeyManager<StreamingAead> streamingAeadKeyManager = streamingAeadKeyManager(str);
            if (streamingAeadKeyManager.getVersion() >= i) {
                return streamingAeadKeyManager;
            }
            throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", new Object[]{str, Integer.valueOf(i)}));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", new Object[]{str2}));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0040  */
    private KeyManager<StreamingAead> streamingAeadKeyManager(String str) throws GeneralSecurityException {
        char c;
        int hashCode = str.hashCode();
        if (hashCode != -2002307740) {
            if (hashCode == -608502222 && str.equals("type.googleapis.com/google.crypto.tink.AesGcmHkdfStreamingKey")) {
                c = 1;
                if (c == 0) {
                    return new AesCtrHmacStreamingKeyManager();
                }
                if (c == 1) {
                    return new AesGcmHkdfStreamingKeyManager();
                }
                throw new GeneralSecurityException(String.format("No support for primitive 'StreamingAead' with key type '%s'.", new Object[]{str}));
            }
        } else if (str.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacStreamingKey")) {
            c = 0;
            if (c == 0) {
            }
        }
        c = 65535;
        if (c == 0) {
        }
    }
}
