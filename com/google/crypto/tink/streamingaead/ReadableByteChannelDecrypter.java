package com.google.crypto.tink.streamingaead;

import com.google.crypto.tink.PrimitiveSet;
import com.google.crypto.tink.StreamingAead;
import com.google.crypto.tink.subtle.RewindableReadableByteChannel;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.security.GeneralSecurityException;

final class ReadableByteChannelDecrypter implements ReadableByteChannel {
    byte[] associatedData;
    boolean attemptedMatching = false;
    RewindableReadableByteChannel ciphertextChannel;
    ReadableByteChannel matchingChannel = null;
    PrimitiveSet<StreamingAead> primitives;

    public ReadableByteChannelDecrypter(PrimitiveSet<StreamingAead> primitiveSet, ReadableByteChannel readableByteChannel, byte[] bArr) {
        this.primitives = primitiveSet;
        this.ciphertextChannel = new RewindableReadableByteChannel(readableByteChannel);
        this.associatedData = (byte[]) bArr.clone();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0059, code lost:
        return r3;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x005a */
    public synchronized int read(ByteBuffer byteBuffer) throws IOException {
        if (byteBuffer.remaining() == 0) {
            return 0;
        }
        if (this.matchingChannel != null) {
            return this.matchingChannel.read(byteBuffer);
        } else if (!this.attemptedMatching) {
            this.attemptedMatching = true;
            try {
                for (PrimitiveSet.Entry primitive : this.primitives.getRawPrimitives()) {
                    ReadableByteChannel newDecryptingChannel = ((StreamingAead) primitive.getPrimitive()).newDecryptingChannel(this.ciphertextChannel, this.associatedData);
                    int read = newDecryptingChannel.read(byteBuffer);
                    if (read > 0) {
                        this.matchingChannel = newDecryptingChannel;
                        this.ciphertextChannel.disableRewinding();
                    } else if (read == 0) {
                        this.ciphertextChannel.rewind();
                        this.attemptedMatching = false;
                    }
                }
                throw new IOException("No matching key found for the ciphertext in the stream.");
            } catch (GeneralSecurityException e) {
                throw new IOException("Keyset failure: ", e);
            } catch (IOException unused) {
                this.ciphertextChannel.rewind();
            } catch (GeneralSecurityException ) {
                this.ciphertextChannel.rewind();
            }
        } else {
            throw new IOException("No matching key found for the ciphertext in the stream.");
        }
    }

    public synchronized void close() throws IOException {
        this.ciphertextChannel.close();
    }

    public synchronized boolean isOpen() {
        return this.ciphertextChannel.isOpen();
    }
}
