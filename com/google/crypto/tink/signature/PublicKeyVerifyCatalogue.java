package com.google.crypto.tink.signature;

import com.google.crypto.tink.Catalogue;
import com.google.crypto.tink.KeyManager;
import com.google.crypto.tink.PublicKeyVerify;
import java.security.GeneralSecurityException;

class PublicKeyVerifyCatalogue implements Catalogue<PublicKeyVerify> {
    public KeyManager<PublicKeyVerify> getKeyManager(String str, String str2, int i) throws GeneralSecurityException {
        String lowerCase = str2.toLowerCase();
        if (((lowerCase.hashCode() == 1712166735 && lowerCase.equals("publickeyverify")) ? (char) 0 : 65535) == 0) {
            KeyManager<PublicKeyVerify> publicKeyVerifyKeyManager = publicKeyVerifyKeyManager(str);
            if (publicKeyVerifyKeyManager.getVersion() >= i) {
                return publicKeyVerifyKeyManager;
            }
            throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", new Object[]{str, Integer.valueOf(i)}));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", new Object[]{str2}));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0040  */
    private KeyManager<PublicKeyVerify> publicKeyVerifyKeyManager(String str) throws GeneralSecurityException {
        char c;
        int hashCode = str.hashCode();
        if (hashCode != 248906128) {
            if (hashCode == 1737079689 && str.equals("type.googleapis.com/google.crypto.tink.Ed25519PublicKey")) {
                c = 1;
                if (c == 0) {
                    return new EcdsaVerifyKeyManager();
                }
                if (c == 1) {
                    return new Ed25519PublicKeyManager();
                }
                throw new GeneralSecurityException(String.format("No support for primitive 'PublicKeyVerify' with key type '%s'.", new Object[]{str}));
            }
        } else if (str.equals("type.googleapis.com/google.crypto.tink.EcdsaPublicKey")) {
            c = 0;
            if (c == 0) {
            }
        }
        c = 65535;
        if (c == 0) {
        }
    }
}
