package com.google.crypto.tink.signature;

import com.google.crypto.tink.Catalogue;
import com.google.crypto.tink.KeyManager;
import com.google.crypto.tink.PublicKeySign;
import java.security.GeneralSecurityException;

class PublicKeySignCatalogue implements Catalogue<PublicKeySign> {
    public KeyManager<PublicKeySign> getKeyManager(String str, String str2, int i) throws GeneralSecurityException {
        String lowerCase = str2.toLowerCase();
        if (((lowerCase.hashCode() == -1213945325 && lowerCase.equals("publickeysign")) ? (char) 0 : 65535) == 0) {
            KeyManager<PublicKeySign> publicKeySignKeyManager = publicKeySignKeyManager(str);
            if (publicKeySignKeyManager.getVersion() >= i) {
                return publicKeySignKeyManager;
            }
            throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", new Object[]{str, Integer.valueOf(i)}));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", new Object[]{str2}));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0040  */
    private KeyManager<PublicKeySign> publicKeySignKeyManager(String str) throws GeneralSecurityException {
        char c;
        int hashCode = str.hashCode();
        if (hashCode != -1470419991) {
            if (hashCode == -359160126 && str.equals("type.googleapis.com/google.crypto.tink.EcdsaPrivateKey")) {
                c = 0;
                if (c == 0) {
                    return new EcdsaSignKeyManager();
                }
                if (c == 1) {
                    return new Ed25519PrivateKeyManager();
                }
                throw new GeneralSecurityException(String.format("No support for primitive 'PublicKeySign' with key type '%s'.", new Object[]{str}));
            }
        } else if (str.equals("type.googleapis.com/google.crypto.tink.Ed25519PrivateKey")) {
            c = 1;
            if (c == 0) {
            }
        }
        c = 65535;
        if (c == 0) {
        }
    }
}
