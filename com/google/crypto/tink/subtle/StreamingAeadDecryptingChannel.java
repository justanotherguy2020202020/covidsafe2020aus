package com.google.crypto.tink.subtle;

import com.atlassian.mobilekit.module.core.utils.StringUtils;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.security.GeneralSecurityException;
import java.util.Arrays;

class StreamingAeadDecryptingChannel implements ReadableByteChannel {
    private static final int PLAINTEXT_SEGMENT_EXTRA_SIZE = 16;
    private byte[] aad;
    private ReadableByteChannel ciphertextChannel;
    private ByteBuffer ciphertextSegment;
    private final int ciphertextSegmentSize;
    private final StreamSegmentDecrypter decrypter;
    private boolean definedState = true;
    private boolean endOfCiphertext = false;
    private boolean endOfPlaintext = false;
    private final int firstCiphertextSegmentSize;
    private ByteBuffer header;
    private boolean headerRead = false;
    private ByteBuffer plaintextSegment;
    private int segmentNr = 0;

    public StreamingAeadDecryptingChannel(NonceBasedStreamingAead nonceBasedStreamingAead, ReadableByteChannel readableByteChannel, byte[] bArr) throws GeneralSecurityException, IOException {
        this.decrypter = nonceBasedStreamingAead.newStreamSegmentDecrypter();
        this.ciphertextChannel = readableByteChannel;
        this.header = ByteBuffer.allocate(nonceBasedStreamingAead.getHeaderLength());
        this.aad = Arrays.copyOf(bArr, bArr.length);
        int ciphertextSegmentSize2 = nonceBasedStreamingAead.getCiphertextSegmentSize();
        this.ciphertextSegmentSize = ciphertextSegmentSize2;
        ByteBuffer allocate = ByteBuffer.allocate(ciphertextSegmentSize2 + 1);
        this.ciphertextSegment = allocate;
        allocate.limit(0);
        this.firstCiphertextSegmentSize = this.ciphertextSegmentSize - nonceBasedStreamingAead.getCiphertextOffset();
        ByteBuffer allocate2 = ByteBuffer.allocate(nonceBasedStreamingAead.getPlaintextSegmentSize() + 16);
        this.plaintextSegment = allocate2;
        allocate2.limit(0);
    }

    /*  JADX ERROR: StackOverflow in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    private void readSomeCiphertext(java.nio.ByteBuffer r3) throws java.io.IOException {
        /*
            r2 = this;
        L_0x0000:
            java.nio.channels.ReadableByteChannel r0 = r2.ciphertextChannel
            int r0 = r0.read(r3)
            if (r0 <= 0) goto L_0x000e
            int r1 = r3.remaining()
            if (r1 > 0) goto L_0x0000
        L_0x000e:
            r3 = -1
            if (r0 != r3) goto L_0x0014
            r3 = 1
            r2.endOfCiphertext = r3
        L_0x0014:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.subtle.StreamingAeadDecryptingChannel.readSomeCiphertext(java.nio.ByteBuffer):void");
    }

    private boolean tryReadHeader() throws IOException {
        if (!this.endOfCiphertext) {
            readSomeCiphertext(this.header);
            if (this.header.remaining() > 0) {
                return false;
            }
            this.header.flip();
            try {
                this.decrypter.init(this.header, this.aad);
                this.headerRead = true;
                return true;
            } catch (GeneralSecurityException e) {
                setUndefinedState();
                throw new IOException(e);
            }
        } else {
            throw new IOException("Ciphertext is too short");
        }
    }

    private void setUndefinedState() {
        this.definedState = false;
        this.plaintextSegment.limit(0);
    }

    private boolean tryLoadSegment() throws IOException {
        if (!this.endOfCiphertext) {
            readSomeCiphertext(this.ciphertextSegment);
        }
        byte b = 0;
        if (this.ciphertextSegment.remaining() > 0 && !this.endOfCiphertext) {
            return false;
        }
        if (!this.endOfCiphertext) {
            ByteBuffer byteBuffer = this.ciphertextSegment;
            b = byteBuffer.get(byteBuffer.position() - 1);
            ByteBuffer byteBuffer2 = this.ciphertextSegment;
            byteBuffer2.position(byteBuffer2.position() - 1);
        }
        this.ciphertextSegment.flip();
        this.plaintextSegment.clear();
        try {
            this.decrypter.decryptSegment(this.ciphertextSegment, this.segmentNr, this.endOfCiphertext, this.plaintextSegment);
            this.segmentNr++;
            this.plaintextSegment.flip();
            this.ciphertextSegment.clear();
            if (!this.endOfCiphertext) {
                this.ciphertextSegment.clear();
                this.ciphertextSegment.limit(this.ciphertextSegmentSize + 1);
                this.ciphertextSegment.put(b);
            }
            return true;
        } catch (GeneralSecurityException e) {
            setUndefinedState();
            throw new IOException(e.getMessage() + StringUtils.EOL + toString() + "\nsegmentNr:" + this.segmentNr + " endOfCiphertext:" + this.endOfCiphertext, e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008e, code lost:
        return r7;
     */
    public synchronized int read(ByteBuffer byteBuffer) throws IOException {
        if (this.definedState) {
            if (!this.headerRead) {
                if (!tryReadHeader()) {
                    return 0;
                }
                this.ciphertextSegment.clear();
                this.ciphertextSegment.limit(this.firstCiphertextSegmentSize + 1);
            }
            if (this.endOfPlaintext) {
                return -1;
            }
            int position = byteBuffer.position();
            while (true) {
                if (byteBuffer.remaining() <= 0) {
                    break;
                }
                if (this.plaintextSegment.remaining() == 0) {
                    if (!this.endOfCiphertext) {
                        if (!tryLoadSegment()) {
                            break;
                        }
                    } else {
                        this.endOfPlaintext = true;
                        break;
                    }
                }
                if (this.plaintextSegment.remaining() <= byteBuffer.remaining()) {
                    this.plaintextSegment.remaining();
                    byteBuffer.put(this.plaintextSegment);
                } else {
                    int remaining = byteBuffer.remaining();
                    ByteBuffer duplicate = this.plaintextSegment.duplicate();
                    duplicate.limit(duplicate.position() + remaining);
                    byteBuffer.put(duplicate);
                    this.plaintextSegment.position(this.plaintextSegment.position() + remaining);
                }
            }
            int position2 = byteBuffer.position() - position;
            if (position2 == 0 && this.endOfPlaintext) {
                return -1;
            }
        } else {
            throw new IOException("This StreamingAeadDecryptingChannel is in an undefined state");
        }
    }

    public synchronized void close() throws IOException {
        this.ciphertextChannel.close();
    }

    public synchronized boolean isOpen() {
        return this.ciphertextChannel.isOpen();
    }

    public synchronized String toString() {
        return "StreamingAeadDecryptingChannel" + "\nsegmentNr:" + this.segmentNr + "\nciphertextSegmentSize:" + this.ciphertextSegmentSize + "\nheaderRead:" + this.headerRead + "\nendOfCiphertext:" + this.endOfCiphertext + "\nendOfPlaintext:" + this.endOfPlaintext + "\ndefinedState:" + this.definedState + "\nHeader" + " position:" + this.header.position() + " limit:" + this.header.position() + "\nciphertextSgement" + " position:" + this.ciphertextSegment.position() + " limit:" + this.ciphertextSegment.limit() + "\nplaintextSegment" + " position:" + this.plaintextSegment.position() + " limit:" + this.plaintextSegment.limit();
    }
}
