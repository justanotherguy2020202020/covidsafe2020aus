package com.google.crypto.tink.subtle;

import java.util.Arrays;

final class Field25519 {
    private static final int[] EXPAND_SHIFT = {0, 2, 3, 5, 6, 0, 1, 3, 4, 6};
    private static final int[] EXPAND_START = {0, 3, 6, 9, 12, 16, 19, 22, 25, 28};
    static final int FIELD_LEN = 32;
    static final int LIMB_CNT = 10;
    private static final int[] MASK = {67108863, 33554431};
    private static final int[] SHIFT = {26, 25};
    private static final long TWO_TO_25 = 33554432;
    private static final long TWO_TO_26 = 67108864;

    private static int eq(int i, int i2) {
        int i3 = ~(i ^ i2);
        int i4 = i3 & (i3 << 16);
        int i5 = i4 & (i4 << 8);
        int i6 = i5 & (i5 << 4);
        int i7 = i6 & (i6 << 2);
        return (i7 & (i7 << 1)) >> 31;
    }

    private static int gte(int i, int i2) {
        return ~((i - i2) >> 31);
    }

    Field25519() {
    }

    static void sum(long[] jArr, long[] jArr2, long[] jArr3) {
        for (int i = 0; i < 10; i++) {
            jArr[i] = jArr2[i] + jArr3[i];
        }
    }

    static void sum(long[] jArr, long[] jArr2) {
        sum(jArr, jArr, jArr2);
    }

    static void sub(long[] jArr, long[] jArr2, long[] jArr3) {
        for (int i = 0; i < 10; i++) {
            jArr[i] = jArr2[i] - jArr3[i];
        }
    }

    static void sub(long[] jArr, long[] jArr2) {
        sub(jArr, jArr2, jArr);
    }

    static void scalarProduct(long[] jArr, long[] jArr2, long j) {
        for (int i = 0; i < 10; i++) {
            jArr[i] = jArr2[i] * j;
        }
    }

    /* JADX WARNING: type inference failed for: r18v0, types: [long[]] */
    /* JADX WARNING: Unknown variable types count: 1 */
    static void product(long[] r18, long[] jArr, long[] jArr2) {
        r18[0] = jArr[0] * jArr2[0];
        r18[1] = (jArr[0] * jArr2[1]) + (jArr[1] * jArr2[0]);
        r18[2] = (jArr[1] * 2 * jArr2[1]) + (jArr[0] * jArr2[2]) + (jArr[2] * jArr2[0]);
        r18[3] = (jArr[1] * jArr2[2]) + (jArr[2] * jArr2[1]) + (jArr[0] * jArr2[3]) + (jArr[3] * jArr2[0]);
        r18[4] = (jArr[2] * jArr2[2]) + (((jArr[1] * jArr2[3]) + (jArr[3] * jArr2[1])) * 2) + (jArr[0] * jArr2[4]) + (jArr[4] * jArr2[0]);
        r18[5] = (jArr[2] * jArr2[3]) + (jArr[3] * jArr2[2]) + (jArr[1] * jArr2[4]) + (jArr[4] * jArr2[1]) + (jArr[0] * jArr2[5]) + (jArr[5] * jArr2[0]);
        r18[6] = (((jArr[3] * jArr2[3]) + (jArr[1] * jArr2[5]) + (jArr[5] * jArr2[1])) * 2) + (jArr[2] * jArr2[4]) + (jArr[4] * jArr2[2]) + (jArr[0] * jArr2[6]) + (jArr[6] * jArr2[0]);
        r18[7] = (jArr[3] * jArr2[4]) + (jArr[4] * jArr2[3]) + (jArr[2] * jArr2[5]) + (jArr[5] * jArr2[2]) + (jArr[1] * jArr2[6]) + (jArr[6] * jArr2[1]) + (jArr[0] * jArr2[7]) + (jArr[7] * jArr2[0]);
        r18[8] = (jArr[4] * jArr2[4]) + (((jArr[3] * jArr2[5]) + (jArr[5] * jArr2[3]) + (jArr[1] * jArr2[7]) + (jArr[7] * jArr2[1])) * 2) + (jArr[2] * jArr2[6]) + (jArr[6] * jArr2[2]) + (jArr[0] * jArr2[8]) + (jArr[8] * jArr2[0]);
        r18[9] = (jArr[4] * jArr2[5]) + (jArr[5] * jArr2[4]) + (jArr[3] * jArr2[6]) + (jArr[6] * jArr2[3]) + (jArr[2] * jArr2[7]) + (jArr[7] * jArr2[2]) + (jArr[1] * jArr2[8]) + (jArr[8] * jArr2[1]) + (jArr[0] * jArr2[9]) + (jArr[9] * jArr2[0]);
        r18[10] = (((jArr[5] * jArr2[5]) + (jArr[3] * jArr2[7]) + (jArr[7] * jArr2[3]) + (jArr[1] * jArr2[9]) + (jArr[9] * jArr2[1])) * 2) + (jArr[4] * jArr2[6]) + (jArr[6] * jArr2[4]) + (jArr[2] * jArr2[8]) + (jArr[8] * jArr2[2]);
        r18[11] = (jArr[5] * jArr2[6]) + (jArr[6] * jArr2[5]) + (jArr[4] * jArr2[7]) + (jArr[7] * jArr2[4]) + (jArr[3] * jArr2[8]) + (jArr[8] * jArr2[3]) + (jArr[2] * jArr2[9]) + (jArr[9] * jArr2[2]);
        r18[12] = (jArr[6] * jArr2[6]) + (((jArr[5] * jArr2[7]) + (jArr[7] * jArr2[5]) + (jArr[3] * jArr2[9]) + (jArr[9] * jArr2[3])) * 2) + (jArr[4] * jArr2[8]) + (jArr[8] * jArr2[4]);
        r18[13] = (jArr[6] * jArr2[7]) + (jArr[7] * jArr2[6]) + (jArr[5] * jArr2[8]) + (jArr[8] * jArr2[5]) + (jArr[4] * jArr2[9]) + (jArr[9] * jArr2[4]);
        r18[14] = (((jArr[7] * jArr2[7]) + (jArr[5] * jArr2[9]) + (jArr[9] * jArr2[5])) * 2) + (jArr[6] * jArr2[8]) + (jArr[8] * jArr2[6]);
        r18[15] = (jArr[7] * jArr2[8]) + (jArr[8] * jArr2[7]) + (jArr[6] * jArr2[9]) + (jArr[9] * jArr2[6]);
        r18[16] = (jArr[8] * jArr2[8]) + (((jArr[7] * jArr2[9]) + (jArr[9] * jArr2[7])) * 2);
        r18[17] = (jArr[8] * jArr2[9]) + (jArr[9] * jArr2[8]);
        r18[18] = jArr[9] * 2 * jArr2[9];
    }

    static void reduceSizeByModularReduction(long[] jArr) {
        jArr[8] = jArr[8] + (jArr[18] << 4);
        jArr[8] = jArr[8] + (jArr[18] << 1);
        jArr[8] = jArr[8] + jArr[18];
        jArr[7] = jArr[7] + (jArr[17] << 4);
        jArr[7] = jArr[7] + (jArr[17] << 1);
        jArr[7] = jArr[7] + jArr[17];
        jArr[6] = jArr[6] + (jArr[16] << 4);
        jArr[6] = jArr[6] + (jArr[16] << 1);
        jArr[6] = jArr[6] + jArr[16];
        jArr[5] = jArr[5] + (jArr[15] << 4);
        jArr[5] = jArr[5] + (jArr[15] << 1);
        jArr[5] = jArr[5] + jArr[15];
        jArr[4] = jArr[4] + (jArr[14] << 4);
        jArr[4] = jArr[4] + (jArr[14] << 1);
        jArr[4] = jArr[4] + jArr[14];
        jArr[3] = jArr[3] + (jArr[13] << 4);
        jArr[3] = jArr[3] + (jArr[13] << 1);
        jArr[3] = jArr[3] + jArr[13];
        jArr[2] = jArr[2] + (jArr[12] << 4);
        jArr[2] = jArr[2] + (jArr[12] << 1);
        jArr[2] = jArr[2] + jArr[12];
        jArr[1] = jArr[1] + (jArr[11] << 4);
        jArr[1] = jArr[1] + (jArr[11] << 1);
        jArr[1] = jArr[1] + jArr[11];
        jArr[0] = jArr[0] + (jArr[10] << 4);
        jArr[0] = jArr[0] + (jArr[10] << 1);
        jArr[0] = jArr[0] + jArr[10];
    }

    static void reduceCoefficients(long[] jArr) {
        jArr[10] = 0;
        int i = 0;
        while (i < 10) {
            long j = jArr[i] / TWO_TO_26;
            jArr[i] = jArr[i] - (j << 26);
            int i2 = i + 1;
            jArr[i2] = jArr[i2] + j;
            long j2 = jArr[i2] / TWO_TO_25;
            jArr[i2] = jArr[i2] - (j2 << 25);
            i += 2;
            jArr[i] = jArr[i] + j2;
        }
        jArr[0] = jArr[0] + (jArr[10] << 4);
        jArr[0] = jArr[0] + (jArr[10] << 1);
        jArr[0] = jArr[0] + jArr[10];
        jArr[10] = 0;
        long j3 = jArr[0] / TWO_TO_26;
        jArr[0] = jArr[0] - (j3 << 26);
        jArr[1] = jArr[1] + j3;
    }

    static void mult(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] jArr4 = new long[19];
        product(jArr4, jArr2, jArr3);
        reduceSizeByModularReduction(jArr4);
        reduceCoefficients(jArr4);
        System.arraycopy(jArr4, 0, jArr, 0, 10);
    }

    /* JADX WARNING: type inference failed for: r23v0, types: [long[]] */
    /* JADX WARNING: Unknown variable types count: 1 */
    private static void squareInner(long[] r23, long[] jArr) {
        r23[0] = jArr[0] * jArr[0];
        r23[1] = jArr[0] * 2 * jArr[1];
        r23[2] = ((jArr[1] * jArr[1]) + (jArr[0] * jArr[2])) * 2;
        r23[3] = ((jArr[1] * jArr[2]) + (jArr[0] * jArr[3])) * 2;
        r23[4] = (jArr[2] * jArr[2]) + (jArr[1] * 4 * jArr[3]) + (jArr[0] * 2 * jArr[4]);
        r23[5] = ((jArr[2] * jArr[3]) + (jArr[1] * jArr[4]) + (jArr[0] * jArr[5])) * 2;
        r23[6] = ((jArr[3] * jArr[3]) + (jArr[2] * jArr[4]) + (jArr[0] * jArr[6]) + (jArr[1] * 2 * jArr[5])) * 2;
        r23[7] = ((jArr[3] * jArr[4]) + (jArr[2] * jArr[5]) + (jArr[1] * jArr[6]) + (jArr[0] * jArr[7])) * 2;
        r23[8] = (jArr[4] * jArr[4]) + (((jArr[2] * jArr[6]) + (jArr[0] * jArr[8]) + (((jArr[1] * jArr[7]) + (jArr[3] * jArr[5])) * 2)) * 2);
        r23[9] = ((jArr[4] * jArr[5]) + (jArr[3] * jArr[6]) + (jArr[2] * jArr[7]) + (jArr[1] * jArr[8]) + (jArr[0] * jArr[9])) * 2;
        r23[10] = ((jArr[5] * jArr[5]) + (jArr[4] * jArr[6]) + (jArr[2] * jArr[8]) + (((jArr[3] * jArr[7]) + (jArr[1] * jArr[9])) * 2)) * 2;
        r23[11] = ((jArr[5] * jArr[6]) + (jArr[4] * jArr[7]) + (jArr[3] * jArr[8]) + (jArr[2] * jArr[9])) * 2;
        r23[12] = (jArr[6] * jArr[6]) + (((jArr[4] * jArr[8]) + (((jArr[5] * jArr[7]) + (jArr[3] * jArr[9])) * 2)) * 2);
        r23[13] = ((jArr[6] * jArr[7]) + (jArr[5] * jArr[8]) + (jArr[4] * jArr[9])) * 2;
        r23[14] = ((jArr[7] * jArr[7]) + (jArr[6] * jArr[8]) + (jArr[5] * 2 * jArr[9])) * 2;
        r23[15] = ((jArr[7] * jArr[8]) + (jArr[6] * jArr[9])) * 2;
        r23[16] = (jArr[8] * jArr[8]) + (jArr[7] * 4 * jArr[9]);
        r23[17] = jArr[8] * 2 * jArr[9];
        r23[18] = jArr[9] * 2 * jArr[9];
    }

    static void square(long[] jArr, long[] jArr2) {
        long[] jArr3 = new long[19];
        squareInner(jArr3, jArr2);
        reduceSizeByModularReduction(jArr3);
        reduceCoefficients(jArr3);
        System.arraycopy(jArr3, 0, jArr, 0, 10);
    }

    static long[] expand(byte[] bArr) {
        long[] jArr = new long[10];
        for (int i = 0; i < 10; i++) {
            int[] iArr = EXPAND_START;
            jArr[i] = ((((((long) (bArr[iArr[i]] & 255)) | (((long) (bArr[iArr[i] + 1] & 255)) << 8)) | (((long) (bArr[iArr[i] + 2] & 255)) << 16)) | (((long) (bArr[iArr[i] + 3] & 255)) << 24)) >> EXPAND_SHIFT[i]) & ((long) MASK[i & 1]);
        }
        return jArr;
    }

    static byte[] contract(long[] jArr) {
        long[] copyOf = Arrays.copyOf(jArr, 10);
        for (int i = 0; i < 2; i++) {
            int i2 = 0;
            while (i2 < 9) {
                long j = copyOf[i2] & (copyOf[i2] >> 31);
                int[] iArr = SHIFT;
                int i3 = i2 & 1;
                int i4 = -((int) (j >> iArr[i3]));
                copyOf[i2] = copyOf[i2] + ((long) (i4 << iArr[i3]));
                i2++;
                copyOf[i2] = copyOf[i2] - ((long) i4);
            }
            int i5 = -((int) (((copyOf[9] >> 31) & copyOf[9]) >> 25));
            copyOf[9] = copyOf[9] + ((long) (i5 << 25));
            copyOf[0] = copyOf[0] - ((long) (i5 * 19));
        }
        int i6 = -((int) ((copyOf[0] & (copyOf[0] >> 31)) >> 26));
        copyOf[0] = copyOf[0] + ((long) (i6 << 26));
        copyOf[1] = copyOf[1] - ((long) i6);
        for (int i7 = 0; i7 < 2; i7++) {
            int i8 = 0;
            while (i8 < 9) {
                int i9 = i8 & 1;
                copyOf[i8] = copyOf[i8] & ((long) MASK[i9]);
                i8++;
                copyOf[i8] = copyOf[i8] + ((long) ((int) (copyOf[i8] >> SHIFT[i9])));
            }
        }
        copyOf[9] = copyOf[9] & 33554431;
        copyOf[0] = copyOf[0] + ((long) (((int) (copyOf[9] >> 25)) * 19));
        int gte = gte((int) copyOf[0], 67108845);
        for (int i10 = 1; i10 < 10; i10++) {
            gte &= eq((int) copyOf[i10], MASK[i10 & 1]);
        }
        copyOf[0] = copyOf[0] - ((long) (67108845 & gte));
        long j2 = (long) (33554431 & gte);
        copyOf[1] = copyOf[1] - j2;
        for (int i11 = 2; i11 < 10; i11 += 2) {
            copyOf[i11] = copyOf[i11] - ((long) (67108863 & gte));
            int i12 = i11 + 1;
            copyOf[i12] = copyOf[i12] - j2;
        }
        for (int i13 = 0; i13 < 10; i13++) {
            copyOf[i13] = copyOf[i13] << EXPAND_SHIFT[i13];
        }
        byte[] bArr = new byte[32];
        for (int i14 = 0; i14 < 10; i14++) {
            int[] iArr2 = EXPAND_START;
            int i15 = iArr2[i14];
            bArr[i15] = (byte) ((int) (((long) bArr[i15]) | (copyOf[i14] & 255)));
            int i16 = iArr2[i14] + 1;
            bArr[i16] = (byte) ((int) (((long) bArr[i16]) | ((copyOf[i14] >> 8) & 255)));
            int i17 = iArr2[i14] + 2;
            bArr[i17] = (byte) ((int) (((long) bArr[i17]) | ((copyOf[i14] >> 16) & 255)));
            int i18 = iArr2[i14] + 3;
            bArr[i18] = (byte) ((int) (((long) bArr[i18]) | ((copyOf[i14] >> 24) & 255)));
        }
        return bArr;
    }

    static void inverse(long[] jArr, long[] jArr2) {
        long[] jArr3 = new long[10];
        long[] jArr4 = new long[10];
        long[] jArr5 = new long[10];
        long[] jArr6 = new long[10];
        long[] jArr7 = new long[10];
        long[] jArr8 = new long[10];
        long[] jArr9 = new long[10];
        long[] jArr10 = new long[10];
        long[] jArr11 = new long[10];
        long[] jArr12 = new long[10];
        square(jArr3, jArr2);
        square(jArr12, jArr3);
        square(jArr11, jArr12);
        mult(jArr4, jArr11, jArr2);
        mult(jArr5, jArr4, jArr3);
        square(jArr11, jArr5);
        mult(jArr6, jArr11, jArr4);
        square(jArr11, jArr6);
        square(jArr12, jArr11);
        square(jArr11, jArr12);
        square(jArr12, jArr11);
        square(jArr11, jArr12);
        mult(jArr7, jArr11, jArr6);
        square(jArr11, jArr7);
        square(jArr12, jArr11);
        for (int i = 2; i < 10; i += 2) {
            square(jArr11, jArr12);
            square(jArr12, jArr11);
        }
        mult(jArr8, jArr12, jArr7);
        square(jArr11, jArr8);
        square(jArr12, jArr11);
        for (int i2 = 2; i2 < 20; i2 += 2) {
            square(jArr11, jArr12);
            square(jArr12, jArr11);
        }
        mult(jArr11, jArr12, jArr8);
        square(jArr12, jArr11);
        square(jArr11, jArr12);
        for (int i3 = 2; i3 < 10; i3 += 2) {
            square(jArr12, jArr11);
            square(jArr11, jArr12);
        }
        mult(jArr9, jArr11, jArr7);
        square(jArr11, jArr9);
        square(jArr12, jArr11);
        for (int i4 = 2; i4 < 50; i4 += 2) {
            square(jArr11, jArr12);
            square(jArr12, jArr11);
        }
        mult(jArr10, jArr12, jArr9);
        square(jArr12, jArr10);
        square(jArr11, jArr12);
        for (int i5 = 2; i5 < 100; i5 += 2) {
            square(jArr12, jArr11);
            square(jArr11, jArr12);
        }
        mult(jArr12, jArr11, jArr10);
        square(jArr11, jArr12);
        square(jArr12, jArr11);
        for (int i6 = 2; i6 < 50; i6 += 2) {
            square(jArr11, jArr12);
            square(jArr12, jArr11);
        }
        mult(jArr11, jArr12, jArr9);
        square(jArr12, jArr11);
        square(jArr11, jArr12);
        square(jArr12, jArr11);
        square(jArr11, jArr12);
        square(jArr12, jArr11);
        mult(jArr, jArr12, jArr5);
    }
}
