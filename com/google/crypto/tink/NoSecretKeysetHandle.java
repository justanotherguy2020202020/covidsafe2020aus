package com.google.crypto.tink;

import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.Keyset;
import com.google.protobuf.InvalidProtocolBufferException;
import java.io.IOException;
import java.security.GeneralSecurityException;

public final class NoSecretKeysetHandle {
    @Deprecated
    public static final KeysetHandle parseFrom(byte[] bArr) throws GeneralSecurityException {
        try {
            Keyset parseFrom = Keyset.parseFrom(bArr);
            validate(parseFrom);
            return KeysetHandle.fromKeyset(parseFrom);
        } catch (InvalidProtocolBufferException unused) {
            throw new GeneralSecurityException("invalid keyset");
        }
    }

    public static final KeysetHandle read(KeysetReader keysetReader) throws GeneralSecurityException, IOException {
        Keyset read = keysetReader.read();
        validate(read);
        return KeysetHandle.fromKeyset(read);
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000e  */
    private static void validate(Keyset keyset) throws GeneralSecurityException {
        for (Keyset.Key next : keyset.getKeyList()) {
            if (next.getKeyData().getKeyMaterialType() == KeyData.KeyMaterialType.UNKNOWN_KEYMATERIAL || next.getKeyData().getKeyMaterialType() == KeyData.KeyMaterialType.SYMMETRIC || next.getKeyData().getKeyMaterialType() == KeyData.KeyMaterialType.ASYMMETRIC_PRIVATE) {
                throw new GeneralSecurityException("keyset contains secret key material");
            }
            while (r3.hasNext()) {
            }
        }
    }
}
