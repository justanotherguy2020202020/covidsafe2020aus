package rx.internal.reactivestreams;

import java.util.ArrayList;
import java.util.List;
import rx.Producer;
import rx.Subscription;

public final class RxJavaSynchronizedProducer implements Producer, Subscription {
    private boolean emitting;
    private List<Long> requests;
    private final org.reactivestreams.Subscription subscription;
    private volatile boolean unsubscribed;

    public RxJavaSynchronizedProducer(org.reactivestreams.Subscription subscription2) {
        if (subscription2 != null) {
            this.subscription = subscription2;
            return;
        }
        throw new NullPointerException("subscription");
    }

    public boolean isUnsubscribed() {
        return this.unsubscribed;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r7.subscription.request(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0036, code lost:
        monitor-enter(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r8 = r7.requests;
        r7.requests = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003c, code lost:
        if (r8 != null) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003e, code lost:
        r7.emitting = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        monitor-exit(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0041, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        monitor-exit(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r8 = r8.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x004b, code lost:
        if (r8.hasNext() == false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x004d, code lost:
        r9 = r8.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0059, code lost:
        if (r9.longValue() != 0) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x005b, code lost:
        r7.unsubscribed = true;
        r7.subscription.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0062, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0063, code lost:
        r7.subscription.request(r9.longValue());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x006d, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x006e, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        monitor-exit(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        throw r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0071, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0073, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0075, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0076, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0077, code lost:
        if (r2 == false) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0079, code lost:
        monitor-enter(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        r7.emitting = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0081, code lost:
        throw r8;
     */
    public void request(long j) {
        if (j > 0 && !this.unsubscribed) {
            synchronized (this) {
                if (!this.unsubscribed) {
                    if (this.emitting) {
                        if (this.requests == null) {
                            this.requests = new ArrayList(4);
                        }
                        this.requests.add(Long.valueOf(j));
                        return;
                    }
                    boolean z = true;
                    this.emitting = true;
                }
            }
        }
    }

    public void unsubscribe() {
        if (!this.unsubscribed) {
            synchronized (this) {
                if (!this.unsubscribed) {
                    if (this.emitting) {
                        ArrayList arrayList = new ArrayList(4);
                        this.requests = arrayList;
                        arrayList.add(0L);
                        return;
                    }
                    this.emitting = true;
                    this.unsubscribed = true;
                    this.subscription.cancel();
                }
            }
        }
    }
}
