package rx.internal.util;

import java.util.concurrent.atomic.AtomicLong;
import kotlin.jvm.internal.LongCompanionObject;
import rx.Producer;

public final class BackpressureDrainManager extends AtomicLong implements Producer {
    private static final long serialVersionUID = 2826241102729529449L;
    final BackpressureQueueCallback actual;
    boolean emitting;
    Throwable exception;
    volatile boolean terminated;

    public interface BackpressureQueueCallback {
        boolean accept(Object obj);

        void complete(Throwable th);

        Object peek();

        Object poll();
    }

    public BackpressureDrainManager(BackpressureQueueCallback backpressureQueueCallback) {
        this.actual = backpressureQueueCallback;
    }

    public boolean isTerminated() {
        return this.terminated;
    }

    public void terminate() {
        this.terminated = true;
    }

    public void terminate(Throwable th) {
        if (!this.terminated) {
            this.exception = th;
            this.terminated = true;
        }
    }

    public void terminateAndDrain() {
        this.terminated = true;
        drain();
    }

    public void terminateAndDrain(Throwable th) {
        if (!this.terminated) {
            this.exception = th;
            this.terminated = true;
            drain();
        }
    }

    public void request(long j) {
        boolean z;
        if (j != 0) {
            while (true) {
                long j2 = get();
                boolean z2 = true;
                z = j2 == 0;
                long j3 = LongCompanionObject.MAX_VALUE;
                if (j2 == LongCompanionObject.MAX_VALUE) {
                    break;
                }
                if (j == LongCompanionObject.MAX_VALUE) {
                    j3 = j;
                } else {
                    if (j2 <= LongCompanionObject.MAX_VALUE - j) {
                        j3 = j2 + j;
                    }
                    z2 = z;
                }
                if (compareAndSet(j2, j3)) {
                    z = z2;
                    break;
                }
            }
            if (z) {
                drain();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r5 = r13.actual;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0014, code lost:
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0015, code lost:
        r9 = (r2 > 0 ? 1 : (r2 == 0 ? 0 : -1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        if (r9 > 0) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001b, code lost:
        if (r1 == false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001d, code lost:
        if (r1 == false) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0023, code lost:
        if (r5.peek() != null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r5.complete(r13.exception);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002a, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002b, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002e, code lost:
        if (r9 != 0) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r9 = r5.poll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0035, code lost:
        if (r9 != null) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0037, code lost:
        monitor-enter(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r1 = r13.terminated;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x003e, code lost:
        if (r5.peek() == null) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0040, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0042, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x004e, code lost:
        if (get() != kotlin.jvm.internal.LongCompanionObject.MAX_VALUE) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0050, code lost:
        if (r2 != false) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0052, code lost:
        if (r1 != false) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r13.emitting = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0056, code lost:
        monitor-exit(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0057, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0058, code lost:
        r2 = Long.MAX_VALUE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r9 = addAndGet((long) (-r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0062, code lost:
        if (r9 == 0) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0064, code lost:
        if (r2 != false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0066, code lost:
        if (r1 == false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0068, code lost:
        if (r2 == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x006b, code lost:
        r2 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x006c, code lost:
        monitor-exit(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r13.emitting = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0070, code lost:
        monitor-exit(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0071, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0072, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0073, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0074, code lost:
        monitor-exit(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0076, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x007c, code lost:
        if (r5.accept(r9) == false) goto L_0x007f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x007e, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x007f, code lost:
        r2 = r2 - 1;
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0085, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0086, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0087, code lost:
        if (r0 == false) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0089, code lost:
        monitor-enter(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        r13.emitting = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0091, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000d, code lost:
        r2 = get();
     */
    public void drain() {
        synchronized (this) {
            if (!this.emitting) {
                boolean z = true;
                this.emitting = true;
                boolean z2 = this.terminated;
            }
        }
    }
}
