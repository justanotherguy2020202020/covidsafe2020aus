package au.gov.health.covidsafe.streetpass;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import au.gov.health.covidsafe.BuildConfig;
import au.gov.health.covidsafe.TracerApp;
import au.gov.health.covidsafe.Utils;
import au.gov.health.covidsafe.bluetooth.gatt.GATTKt;
import au.gov.health.covidsafe.bluetooth.gatt.ReadRequestPayload;
import au.gov.health.covidsafe.bluetooth.gatt.WriteRequestPayload;
import au.gov.health.covidsafe.logging.CentralLog;
import au.gov.health.covidsafe.streetpass.Work;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.PriorityBlockingQueue;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\b\u0007\u0018\u00002\u00020\u0001:\u0003012B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0011J\u0006\u0010%\u001a\u00020&J\u000e\u0010'\u001a\u00020&2\u0006\u0010$\u001a\u00020\u0011J\u0010\u0010(\u001a\u00020#2\u0006\u0010)\u001a\u00020*H\u0002J\u0010\u0010+\u001a\u00020#2\b\u0010,\u001a\u0004\u0018\u00010\u0006J\b\u0010-\u001a\u00020&H\u0002J\u0006\u0010.\u001a\u00020&J\u0006\u0010/\u001a\u00020&R\u000e\u0010\u0005\u001a\u00020\u0006XD¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX.¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00060\u0013R\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0016\u001a\u00020\u0017¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u000e\u0010\u001a\u001a\u00020\u000bX.¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u000bX.¢\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00110\u001fX\u0004¢\u0006\u0002\n\u0000R\u0012\u0010 \u001a\u00060!R\u00020\u0000X\u0004¢\u0006\u0002\n\u0000¨\u00063"}, d2 = {"Lau/gov/health/covidsafe/streetpass/StreetPassWorker;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "TAG", "", "blacklist", "", "Lau/gov/health/covidsafe/streetpass/BlacklistEntry;", "blacklistHandler", "Landroid/os/Handler;", "bluetoothManager", "Landroid/bluetooth/BluetoothManager;", "getContext", "()Landroid/content/Context;", "currentPendingConnection", "Lau/gov/health/covidsafe/streetpass/Work;", "deviceProcessedReceiver", "Lau/gov/health/covidsafe/streetpass/StreetPassWorker$DeviceProcessedReceiver;", "localBroadcastManager", "Landroidx/localbroadcastmanager/content/LocalBroadcastManager;", "onWorkTimeoutListener", "Lau/gov/health/covidsafe/streetpass/Work$OnWorkTimeoutListener;", "getOnWorkTimeoutListener", "()Lau/gov/health/covidsafe/streetpass/Work$OnWorkTimeoutListener;", "queueHandler", "serviceUUID", "Ljava/util/UUID;", "timeoutHandler", "workQueue", "Ljava/util/concurrent/PriorityBlockingQueue;", "workReceiver", "Lau/gov/health/covidsafe/streetpass/StreetPassWorker$StreetPassWorkReceiver;", "addWork", "", "work", "doWork", "", "finishWork", "getConnectionStatus", "device", "Landroid/bluetooth/BluetoothDevice;", "isCurrentlyWorkedOn", "address", "prepare", "terminateConnections", "unregisterReceivers", "DeviceProcessedReceiver", "StreetPassGattCallback", "StreetPassWorkReceiver", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StreetPassWorker.kt */
public final class StreetPassWorker {
    /* access modifiers changed from: private */
    public final String TAG;
    /* access modifiers changed from: private */
    public final List<BlacklistEntry> blacklist;
    /* access modifiers changed from: private */
    public Handler blacklistHandler;
    private final BluetoothManager bluetoothManager;
    private final Context context;
    /* access modifiers changed from: private */
    public Work currentPendingConnection;
    private final DeviceProcessedReceiver deviceProcessedReceiver;
    private LocalBroadcastManager localBroadcastManager;
    private final Work.OnWorkTimeoutListener onWorkTimeoutListener;
    private Handler queueHandler;
    /* access modifiers changed from: private */
    public final UUID serviceUUID;
    /* access modifiers changed from: private */
    public Handler timeoutHandler;
    /* access modifiers changed from: private */
    public final PriorityBlockingQueue<Work> workQueue = new PriorityBlockingQueue<>();
    private final StreetPassWorkReceiver workReceiver;

    public StreetPassWorker(Context context2) {
        Intrinsics.checkParameterIsNotNull(context2, "context");
        this.context = context2;
        List<BlacklistEntry> synchronizedList = Collections.synchronizedList(new ArrayList());
        Intrinsics.checkExpressionValueIsNotNull(synchronizedList, "Collections.synchronizedList(ArrayList())");
        this.blacklist = synchronizedList;
        this.workReceiver = new StreetPassWorkReceiver();
        this.deviceProcessedReceiver = new DeviceProcessedReceiver();
        UUID fromString = UUID.fromString(BuildConfig.BLE_SSID);
        Intrinsics.checkExpressionValueIsNotNull(fromString, "UUID.fromString(BuildConfig.BLE_SSID)");
        this.serviceUUID = fromString;
        this.TAG = "StreetPassWorker";
        Object systemService = this.context.getSystemService("bluetooth");
        if (systemService != null) {
            this.bluetoothManager = (BluetoothManager) systemService;
            LocalBroadcastManager instance = LocalBroadcastManager.getInstance(this.context);
            Intrinsics.checkExpressionValueIsNotNull(instance, "LocalBroadcastManager.getInstance(context)");
            this.localBroadcastManager = instance;
            this.onWorkTimeoutListener = new StreetPassWorker$onWorkTimeoutListener$1(this);
            prepare();
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.bluetooth.BluetoothManager");
    }

    public static final /* synthetic */ Handler access$getBlacklistHandler$p(StreetPassWorker streetPassWorker) {
        Handler handler = streetPassWorker.blacklistHandler;
        if (handler == null) {
            Intrinsics.throwUninitializedPropertyAccessException("blacklistHandler");
        }
        return handler;
    }

    public static final /* synthetic */ Handler access$getTimeoutHandler$p(StreetPassWorker streetPassWorker) {
        Handler handler = streetPassWorker.timeoutHandler;
        if (handler == null) {
            Intrinsics.throwUninitializedPropertyAccessException("timeoutHandler");
        }
        return handler;
    }

    public final Context getContext() {
        return this.context;
    }

    public final Work.OnWorkTimeoutListener getOnWorkTimeoutListener() {
        return this.onWorkTimeoutListener;
    }

    private final void prepare() {
        this.localBroadcastManager.registerReceiver(this.workReceiver, new IntentFilter(StreetPassKt.ACTION_DEVICE_SCANNED));
        this.localBroadcastManager.registerReceiver(this.deviceProcessedReceiver, new IntentFilter(GATTKt.ACTION_DEVICE_PROCESSED));
        this.timeoutHandler = new Handler();
        this.queueHandler = new Handler();
        this.blacklistHandler = new Handler();
    }

    public final boolean isCurrentlyWorkedOn(String str) {
        Work work = this.currentPendingConnection;
        if (work != null) {
            return Intrinsics.areEqual((Object) work.getDevice().getAddress(), (Object) str);
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008f  */
    public final boolean addWork(Work work) {
        boolean z;
        boolean z2;
        Object obj;
        Intrinsics.checkParameterIsNotNull(work, "work");
        if (isCurrentlyWorkedOn(work.getDevice().getAddress())) {
            CentralLog.Companion.i(this.TAG, work.getDevice().getAddress() + " is being worked on, not adding to queue");
            return false;
        }
        Iterable iterable = this.blacklist;
        if (!(iterable instanceof Collection) || !((Collection) iterable).isEmpty()) {
            Iterator it = iterable.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (Intrinsics.areEqual((Object) ((BlacklistEntry) it.next()).getUniqueIdentifier(), (Object) work.getDevice().getAddress())) {
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (!z) {
                CentralLog.Companion.i(this.TAG, work.getDevice().getAddress() + " is in blacklist, not adding to queue");
                return false;
            }
            Iterable iterable2 = this.workQueue;
            if (!(iterable2 instanceof Collection) || !((Collection) iterable2).isEmpty()) {
                Iterator it2 = iterable2.iterator();
                while (true) {
                    if (it2.hasNext()) {
                        if (Intrinsics.areEqual((Object) ((Work) it2.next()).getDevice().getAddress(), (Object) work.getDevice().getAddress())) {
                            z2 = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            z2 = true;
            if (z2) {
                this.workQueue.offer(work);
                Handler handler = this.queueHandler;
                if (handler == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("queueHandler");
                }
                handler.postDelayed(new StreetPassWorker$addWork$3(this, work), 7000);
                CentralLog.Companion.i(this.TAG, "Added to work queue: " + work.getDevice().getAddress());
                return true;
            }
            CentralLog.Companion.i(this.TAG, work.getDevice().getAddress() + " is already in work queue");
            Iterator it3 = this.workQueue.iterator();
            while (true) {
                if (!it3.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it3.next();
                if (Intrinsics.areEqual((Object) ((Work) obj).getDevice().getAddress(), (Object) work.getDevice().getAddress())) {
                    break;
                }
            }
            boolean remove = this.workQueue.remove((Work) obj);
            boolean offer = this.workQueue.offer(work);
            CentralLog.Companion.i(this.TAG, "Queue entry updated - removed: " + remove + ", added: " + offer);
            return false;
        }
        z = false;
        if (!z) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x001b, code lost:
        r6 = r6.getDevice();
     */
    public final void doWork() {
        Work work;
        BluetoothGatt gatt;
        BluetoothDevice device;
        BluetoothDevice device2;
        BluetoothDevice device3;
        boolean z = false;
        boolean z2 = true;
        String str = null;
        if (this.currentPendingConnection != null) {
            CentralLog.Companion companion = CentralLog.Companion;
            String str2 = this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Already trying to connect to: ");
            Work work2 = this.currentPendingConnection;
            String address = (work2 == null || device3 == null) ? null : device3.getAddress();
            sb.append(address);
            companion.i(str2, sb.toString());
            long currentTimeMillis = System.currentTimeMillis();
            Work work3 = this.currentPendingConnection;
            if (currentTimeMillis <= (work3 != null ? work3.getTimeout() : 0)) {
                z2 = false;
            }
            Work work4 = this.currentPendingConnection;
            if ((work4 != null ? work4.getFinished() : false) || z2) {
                CentralLog.Companion companion2 = CentralLog.Companion;
                String str3 = this.TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Handling erroneous current work for ");
                Work work5 = this.currentPendingConnection;
                sb2.append((work5 == null || (device2 = work5.getDevice()) == null) ? null : device2.getAddress());
                sb2.append(" : - finished: ");
                Work work6 = this.currentPendingConnection;
                if (work6 != null) {
                    z = work6.getFinished();
                }
                sb2.append(z);
                sb2.append(", timedout: ");
                sb2.append(z2);
                companion2.w(str3, sb2.toString());
                if (this.currentPendingConnection != null) {
                    List<BluetoothDevice> connectedDevices = this.bluetoothManager.getConnectedDevices(7);
                    Work work7 = this.currentPendingConnection;
                    if (connectedDevices.contains(work7 != null ? work7.getDevice() : null)) {
                        CentralLog.Companion companion3 = CentralLog.Companion;
                        String str4 = this.TAG;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Disconnecting dangling connection to ");
                        Work work8 = this.currentPendingConnection;
                        if (!(work8 == null || (device = work8.getDevice()) == null)) {
                            str = device.getAddress();
                        }
                        sb3.append(str);
                        companion3.w(str4, sb3.toString());
                        Work work9 = this.currentPendingConnection;
                        if (work9 != null && (gatt = work9.getGatt()) != null) {
                            gatt.disconnect();
                            return;
                        }
                        return;
                    }
                    return;
                }
                doWork();
            }
        } else if (this.workQueue.isEmpty()) {
            CentralLog.Companion.i(this.TAG, "Queue empty. Nothing to do.");
        } else {
            CentralLog.Companion.i(this.TAG, "Queue size: " + this.workQueue.size());
            Work work10 = null;
            long currentTimeMillis2 = System.currentTimeMillis();
            loop0:
            while (true) {
                work = work10;
                while (work == null && (!this.workQueue.isEmpty())) {
                    work = this.workQueue.poll();
                    if (work != null && currentTimeMillis2 - work.getTimeStamp() > 7000) {
                        CentralLog.Companion.w(this.TAG, "Work request for " + work.getDevice().getAddress() + " too old. Not doing");
                    }
                }
            }
            if (work != null) {
                BluetoothDevice device4 = work.getDevice();
                Collection arrayList = new ArrayList();
                for (Object next : this.blacklist) {
                    if (Intrinsics.areEqual((Object) ((BlacklistEntry) next).getUniqueIdentifier(), (Object) device4.getAddress())) {
                        arrayList.add(next);
                    }
                }
                if (!((List) arrayList).isEmpty()) {
                    CentralLog.Companion.w(this.TAG, "Already worked on " + device4.getAddress() + ". Skip.");
                    doWork();
                    return;
                }
                boolean connectionStatus = getConnectionStatus(device4);
                CentralLog.Companion.i(this.TAG, "Already connected to " + device4.getAddress() + " : " + connectionStatus);
                if (connectionStatus) {
                    work.getChecklist().getSkipped().setStatus(true);
                    work.getChecklist().getSkipped().setTimePerformed(System.currentTimeMillis());
                    finishWork(work);
                } else if (work != null) {
                    StreetPassGattCallback streetPassGattCallback = new StreetPassGattCallback(this, work);
                    CentralLog.Companion.i(this.TAG, "Starting work - connecting to device: " + device4.getAddress() + " @ " + work.getConnectable().getRssi() + ' ' + (System.currentTimeMillis() - work.getTimeStamp()) + "ms ago");
                    this.currentPendingConnection = work;
                    try {
                        work.getChecklist().getStarted().setStatus(true);
                        work.getChecklist().getStarted().setTimePerformed(System.currentTimeMillis());
                        work.startWork(this.context, streetPassGattCallback);
                        BluetoothGatt gatt2 = work.getGatt();
                        if (gatt2 != null) {
                            z = gatt2.connect();
                        }
                        if (!z) {
                            CentralLog.Companion.e(this.TAG, "not connecting to " + work.getDevice().getAddress() + "??");
                            CentralLog.Companion.e(this.TAG, "Moving on to next task");
                            this.currentPendingConnection = null;
                            doWork();
                            return;
                        }
                        CentralLog.Companion.i(this.TAG, "Connection to " + work.getDevice().getAddress() + " attempt in progress");
                        Handler handler = this.timeoutHandler;
                        if (handler == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("timeoutHandler");
                        }
                        handler.postDelayed(work.getTimeoutRunnable(), 6000);
                        work.setTimeout(System.currentTimeMillis() + 6000);
                        CentralLog.Companion.i(this.TAG, "Timeout scheduled for " + work.getDevice().getAddress());
                    } catch (Throwable th) {
                        CentralLog.Companion.e(this.TAG, "Unexpected error while attempting to connect to " + device4.getAddress() + ": " + th.getLocalizedMessage());
                        CentralLog.Companion.e(this.TAG, "Moving on to next task");
                        this.currentPendingConnection = work10;
                        doWork();
                        return;
                    }
                } else {
                    CentralLog.Companion.e(this.TAG, "Work not started - missing Work Object");
                }
            }
            if (work == null) {
                CentralLog.Companion.i(this.TAG, "No outstanding work");
            }
        }
    }

    private final boolean getConnectionStatus(BluetoothDevice bluetoothDevice) {
        return this.bluetoothManager.getDevicesMatchingConnectionStates(7, new int[]{2}).contains(bluetoothDevice);
    }

    public final void finishWork(Work work) {
        Intrinsics.checkParameterIsNotNull(work, "work");
        if (work.getFinished()) {
            CentralLog.Companion companion = CentralLog.Companion;
            String str = this.TAG;
            companion.i(str, "Work on " + work.getDevice().getAddress() + " already finished and closed");
            return;
        }
        if (work.isCriticalsCompleted()) {
            Utils utils = Utils.INSTANCE;
            Context context2 = this.context;
            String address = work.getDevice().getAddress();
            Intrinsics.checkExpressionValueIsNotNull(address, "work.device.address");
            utils.broadcastDeviceProcessed(context2, address);
        }
        CentralLog.Companion companion2 = CentralLog.Companion;
        String str2 = this.TAG;
        companion2.i(str2, "Work on " + work.getDevice().getAddress() + " stopped in: " + (work.getChecklist().getDisconnected().getTimePerformed() - work.getChecklist().getStarted().getTimePerformed()));
        CentralLog.Companion companion3 = CentralLog.Companion;
        String str3 = this.TAG;
        companion3.i(str3, "Work on " + work.getDevice().getAddress() + " completed?: " + work.isCriticalsCompleted() + ". Connected in: " + (work.getChecklist().getConnected().getTimePerformed() - work.getChecklist().getStarted().getTimePerformed()) + ". connection lasted for: " + (work.getChecklist().getDisconnected().getTimePerformed() - work.getChecklist().getConnected().getTimePerformed()) + ". Status: " + work.getChecklist());
        Handler handler = this.timeoutHandler;
        if (handler == null) {
            Intrinsics.throwUninitializedPropertyAccessException("timeoutHandler");
        }
        handler.removeCallbacks(work.getTimeoutRunnable());
        CentralLog.Companion companion4 = CentralLog.Companion;
        String str4 = this.TAG;
        companion4.i(str4, "Timeout removed for " + work.getDevice().getAddress());
        work.setFinished(true);
        doWork();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J \u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016J \u0010\u000e\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016J\"\u0010\u000f\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\rH\u0016J\"\u0010\u0011\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\rH\u0016J\u0018\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\rH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lau/gov/health/covidsafe/streetpass/StreetPassWorker$StreetPassGattCallback;", "Landroid/bluetooth/BluetoothGattCallback;", "work", "Lau/gov/health/covidsafe/streetpass/Work;", "(Lau/gov/health/covidsafe/streetpass/StreetPassWorker;Lau/gov/health/covidsafe/streetpass/Work;)V", "endWorkConnection", "", "gatt", "Landroid/bluetooth/BluetoothGatt;", "onCharacteristicRead", "characteristic", "Landroid/bluetooth/BluetoothGattCharacteristic;", "status", "", "onCharacteristicWrite", "onConnectionStateChange", "newState", "onMtuChanged", "mtu", "onServicesDiscovered", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StreetPassWorker.kt */
    public final class StreetPassGattCallback extends BluetoothGattCallback {
        final /* synthetic */ StreetPassWorker this$0;
        private final Work work;

        public StreetPassGattCallback(StreetPassWorker streetPassWorker, Work work2) {
            Intrinsics.checkParameterIsNotNull(work2, "work");
            this.this$0 = streetPassWorker;
            this.work = work2;
        }

        private final void endWorkConnection(BluetoothGatt bluetoothGatt) {
            CentralLog.Companion companion = CentralLog.Companion;
            String access$getTAG$p = this.this$0.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Ending connection with: ");
            BluetoothDevice device = bluetoothGatt.getDevice();
            Intrinsics.checkExpressionValueIsNotNull(device, "gatt.device");
            sb.append(device.getAddress());
            companion.i(access$getTAG$p, sb.toString());
            bluetoothGatt.disconnect();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:9:0x011b, code lost:
            r7 = r7.getDevice();
         */
        public void onConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
            BluetoothDevice device;
            if (bluetoothGatt == null) {
                return;
            }
            if (i2 == 0) {
                CentralLog.Companion companion = CentralLog.Companion;
                String access$getTAG$p = this.this$0.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Disconnected from other GATT server - ");
                BluetoothDevice device2 = bluetoothGatt.getDevice();
                Intrinsics.checkExpressionValueIsNotNull(device2, "gatt.device");
                sb.append(device2.getAddress());
                companion.i(access$getTAG$p, sb.toString());
                this.work.getChecklist().getDisconnected().setStatus(true);
                this.work.getChecklist().getDisconnected().setTimePerformed(System.currentTimeMillis());
                StreetPassWorker.access$getTimeoutHandler$p(this.this$0).removeCallbacks(this.work.getTimeoutRunnable());
                CentralLog.Companion companion2 = CentralLog.Companion;
                String access$getTAG$p2 = this.this$0.TAG;
                companion2.i(access$getTAG$p2, "Timeout removed for " + this.work.getDevice().getAddress());
                String address = this.work.getDevice().getAddress();
                Work access$getCurrentPendingConnection$p = this.this$0.currentPendingConnection;
                String address2 = (access$getCurrentPendingConnection$p == null || device == null) ? null : device.getAddress();
                if (Intrinsics.areEqual((Object) address, (Object) address2)) {
                    this.this$0.currentPendingConnection = null;
                }
                bluetoothGatt.close();
                this.this$0.finishWork(this.work);
            } else if (i2 != 2) {
                CentralLog.Companion companion3 = CentralLog.Companion;
                String access$getTAG$p3 = this.this$0.TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Connection status for ");
                BluetoothDevice device3 = bluetoothGatt.getDevice();
                Intrinsics.checkExpressionValueIsNotNull(device3, "gatt.device");
                sb2.append(device3.getAddress());
                sb2.append(": ");
                sb2.append(i2);
                companion3.i(access$getTAG$p3, sb2.toString());
                endWorkConnection(bluetoothGatt);
            } else {
                CentralLog.Companion companion4 = CentralLog.Companion;
                String access$getTAG$p4 = this.this$0.TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Connected to other GATT server - ");
                BluetoothDevice device4 = bluetoothGatt.getDevice();
                Intrinsics.checkExpressionValueIsNotNull(device4, "gatt.device");
                sb3.append(device4.getAddress());
                companion4.i(access$getTAG$p4, sb3.toString());
                bluetoothGatt.requestConnectionPriority(0);
                bluetoothGatt.requestMtu(512);
                this.work.getChecklist().getConnected().setStatus(true);
                this.work.getChecklist().getConnected().setTimePerformed(System.currentTimeMillis());
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:4:0x003e, code lost:
            r4 = r6.getDevice();
         */
        public void onMtuChanged(BluetoothGatt bluetoothGatt, int i, int i2) {
            BluetoothDevice device;
            if (!this.work.getChecklist().getMtuChanged().getStatus()) {
                boolean z = true;
                this.work.getChecklist().getMtuChanged().setStatus(true);
                this.work.getChecklist().getMtuChanged().setTimePerformed(System.currentTimeMillis());
                CentralLog.Companion companion = CentralLog.Companion;
                String access$getTAG$p = this.this$0.TAG;
                StringBuilder sb = new StringBuilder();
                String address = (bluetoothGatt == null || device == null) ? null : device.getAddress();
                sb.append(address);
                sb.append(" MTU is ");
                sb.append(i);
                sb.append(". Was change successful? : ");
                if (i2 != 0) {
                    z = false;
                }
                sb.append(z);
                companion.i(access$getTAG$p, sb.toString());
                if (bluetoothGatt != null) {
                    boolean discoverServices = bluetoothGatt.discoverServices();
                    CentralLog.Companion companion2 = CentralLog.Companion;
                    String access$getTAG$p2 = this.this$0.TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Attempting to start service discovery on ");
                    BluetoothDevice device2 = bluetoothGatt.getDevice();
                    Intrinsics.checkExpressionValueIsNotNull(device2, "gatt.device");
                    sb2.append(device2.getAddress());
                    sb2.append(": ");
                    sb2.append(discoverServices);
                    companion2.i(access$getTAG$p2, sb2.toString());
                }
            }
        }

        public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
            Intrinsics.checkParameterIsNotNull(bluetoothGatt, "gatt");
            if (i != 0) {
                CentralLog.Companion companion = CentralLog.Companion;
                String access$getTAG$p = this.this$0.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("No services discovered on ");
                BluetoothDevice device = bluetoothGatt.getDevice();
                Intrinsics.checkExpressionValueIsNotNull(device, "gatt.device");
                sb.append(device.getAddress());
                companion.w(access$getTAG$p, sb.toString());
                endWorkConnection(bluetoothGatt);
                return;
            }
            CentralLog.Companion companion2 = CentralLog.Companion;
            String access$getTAG$p2 = this.this$0.TAG;
            companion2.i(access$getTAG$p2, "onServicesDiscovered received: BluetoothGatt.GATT_SUCCESS - " + i);
            CentralLog.Companion companion3 = CentralLog.Companion;
            String access$getTAG$p3 = this.this$0.TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Discovered ");
            sb2.append(bluetoothGatt.getServices().size());
            sb2.append(" services on ");
            BluetoothDevice device2 = bluetoothGatt.getDevice();
            Intrinsics.checkExpressionValueIsNotNull(device2, "gatt.device");
            sb2.append(device2.getAddress());
            companion3.i(access$getTAG$p3, sb2.toString());
            BluetoothGattService service = bluetoothGatt.getService(this.this$0.serviceUUID);
            if (service != null) {
                BluetoothGattCharacteristic characteristic = service.getCharacteristic(this.this$0.serviceUUID);
                if (characteristic != null) {
                    boolean readCharacteristic = bluetoothGatt.readCharacteristic(characteristic);
                    CentralLog.Companion companion4 = CentralLog.Companion;
                    String access$getTAG$p4 = this.this$0.TAG;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Attempt to read characteristic of our service on ");
                    BluetoothDevice device3 = bluetoothGatt.getDevice();
                    Intrinsics.checkExpressionValueIsNotNull(device3, "gatt.device");
                    sb3.append(device3.getAddress());
                    sb3.append(": ");
                    sb3.append(readCharacteristic);
                    companion4.i(access$getTAG$p4, sb3.toString());
                } else {
                    CentralLog.Companion companion5 = CentralLog.Companion;
                    String access$getTAG$p5 = this.this$0.TAG;
                    StringBuilder sb4 = new StringBuilder();
                    BluetoothDevice device4 = bluetoothGatt.getDevice();
                    Intrinsics.checkExpressionValueIsNotNull(device4, "gatt.device");
                    sb4.append(device4.getAddress());
                    sb4.append(" does not have our characteristic");
                    companion5.e(access$getTAG$p5, sb4.toString());
                    endWorkConnection(bluetoothGatt);
                }
            }
            if (service == null) {
                CentralLog.Companion companion6 = CentralLog.Companion;
                String access$getTAG$p6 = this.this$0.TAG;
                StringBuilder sb5 = new StringBuilder();
                BluetoothDevice device5 = bluetoothGatt.getDevice();
                Intrinsics.checkExpressionValueIsNotNull(device5, "gatt.device");
                sb5.append(device5.getAddress());
                sb5.append(" does not have our service");
                companion6.e(access$getTAG$p6, sb5.toString());
                endWorkConnection(bluetoothGatt);
            }
        }

        public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            Intrinsics.checkParameterIsNotNull(bluetoothGatt, "gatt");
            Intrinsics.checkParameterIsNotNull(bluetoothGattCharacteristic, "characteristic");
            CentralLog.Companion companion = CentralLog.Companion;
            String access$getTAG$p = this.this$0.TAG;
            companion.i(access$getTAG$p, "Read Status: " + i);
            if (i != 0) {
                CentralLog.Companion companion2 = CentralLog.Companion;
                String access$getTAG$p2 = this.this$0.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to read characteristics from ");
                BluetoothDevice device = bluetoothGatt.getDevice();
                Intrinsics.checkExpressionValueIsNotNull(device, "gatt.device");
                sb.append(device.getAddress());
                sb.append(": ");
                sb.append(i);
                companion2.w(access$getTAG$p2, sb.toString());
            } else {
                CentralLog.Companion companion3 = CentralLog.Companion;
                String access$getTAG$p3 = this.this$0.TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Characteristic read from ");
                BluetoothDevice device2 = bluetoothGatt.getDevice();
                Intrinsics.checkExpressionValueIsNotNull(device2, "gatt.device");
                sb2.append(device2.getAddress());
                sb2.append(": ");
                sb2.append(bluetoothGattCharacteristic.getStringValue(0));
                companion3.i(access$getTAG$p3, sb2.toString());
                if (Intrinsics.areEqual((Object) bluetoothGattCharacteristic.getUuid(), (Object) this.this$0.serviceUUID)) {
                    CentralLog.Companion companion4 = CentralLog.Companion;
                    String access$getTAG$p4 = this.this$0.TAG;
                    companion4.i(access$getTAG$p4, "onCharacteristicRead: " + this.work.getDevice().getAddress() + " - [" + this.work.getConnectable().getRssi() + ']');
                    byte[] value = bluetoothGattCharacteristic.getValue();
                    try {
                        ReadRequestPayload.Companion companion5 = ReadRequestPayload.Companion;
                        Intrinsics.checkExpressionValueIsNotNull(value, "dataBytes");
                        ReadRequestPayload createReadRequestPayload = companion5.createReadRequestPayload(value);
                        Utils.INSTANCE.broadcastStreetPassReceived(this.this$0.getContext(), new ConnectionRecord(createReadRequestPayload.getV(), createReadRequestPayload.getMsg(), createReadRequestPayload.getOrg(), new PeripheralDevice(createReadRequestPayload.getModelP(), this.work.getDevice().getAddress()), TracerApp.Companion.asCentralDevice(), this.work.getConnectable().getRssi(), this.work.getConnectable().getTransmissionPower()));
                    } catch (Throwable th) {
                        CentralLog.Companion companion6 = CentralLog.Companion;
                        String access$getTAG$p5 = this.this$0.TAG;
                        companion6.e(access$getTAG$p5, "Failed to de-serialize request payload object - " + th.getMessage());
                    }
                }
                this.work.getChecklist().getReadCharacteristic().setStatus(true);
                this.work.getChecklist().getReadCharacteristic().setTimePerformed(System.currentTimeMillis());
            }
            if (Utils.INSTANCE.bmValid(this.this$0.getContext())) {
                bluetoothGattCharacteristic.setValue(new WriteRequestPayload(1, TracerApp.Companion.thisDeviceMsg(), "AU_DTA", TracerApp.Companion.asCentralDevice().getModelC(), this.work.getConnectable().getRssi(), this.work.getConnectable().getTransmissionPower()).getPayload());
                boolean writeCharacteristic = bluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic);
                CentralLog.Companion companion7 = CentralLog.Companion;
                String access$getTAG$p6 = this.this$0.TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Attempt to write characteristic to our service on ");
                BluetoothDevice device3 = bluetoothGatt.getDevice();
                Intrinsics.checkExpressionValueIsNotNull(device3, "gatt.device");
                sb3.append(device3.getAddress());
                sb3.append(": ");
                sb3.append(writeCharacteristic);
                companion7.i(access$getTAG$p6, sb3.toString());
                return;
            }
            CentralLog.Companion companion8 = CentralLog.Companion;
            String access$getTAG$p7 = this.this$0.TAG;
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Expired BM. Skipping attempt to write characteristic to our service on ");
            BluetoothDevice device4 = bluetoothGatt.getDevice();
            Intrinsics.checkExpressionValueIsNotNull(device4, "gatt.device");
            sb4.append(device4.getAddress());
            companion8.i(access$getTAG$p7, sb4.toString());
            endWorkConnection(bluetoothGatt);
        }

        public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            Intrinsics.checkParameterIsNotNull(bluetoothGatt, "gatt");
            Intrinsics.checkParameterIsNotNull(bluetoothGattCharacteristic, "characteristic");
            if (i != 0) {
                CentralLog.Companion companion = CentralLog.Companion;
                String access$getTAG$p = this.this$0.TAG;
                companion.i(access$getTAG$p, "Failed to write characteristics: " + i);
            } else {
                CentralLog.Companion.i(this.this$0.TAG, "Characteristic wrote successfully");
                this.work.getChecklist().getWriteCharacteristic().setStatus(true);
                this.work.getChecklist().getWriteCharacteristic().setTimePerformed(System.currentTimeMillis());
            }
            endWorkConnection(bluetoothGatt);
        }
    }

    public final void terminateConnections() {
        BluetoothGatt gatt;
        CentralLog.Companion.d(this.TAG, "Cleaning up worker.");
        Work work = this.currentPendingConnection;
        if (!(work == null || (gatt = work.getGatt()) == null)) {
            gatt.disconnect();
        }
        this.currentPendingConnection = null;
        Handler handler = this.timeoutHandler;
        if (handler == null) {
            Intrinsics.throwUninitializedPropertyAccessException("timeoutHandler");
        }
        handler.removeCallbacksAndMessages((Object) null);
        Handler handler2 = this.queueHandler;
        if (handler2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("queueHandler");
        }
        handler2.removeCallbacksAndMessages((Object) null);
        Handler handler3 = this.blacklistHandler;
        if (handler3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("blacklistHandler");
        }
        handler3.removeCallbacksAndMessages((Object) null);
        this.workQueue.clear();
        this.blacklist.clear();
    }

    public final void unregisterReceivers() {
        try {
            this.localBroadcastManager.unregisterReceiver(this.deviceProcessedReceiver);
        } catch (Throwable th) {
            CentralLog.Companion companion = CentralLog.Companion;
            String str = this.TAG;
            companion.e(str, "Unable to close receivers: " + th.getLocalizedMessage());
        }
        try {
            this.localBroadcastManager.unregisterReceiver(this.workReceiver);
        } catch (Throwable th2) {
            CentralLog.Companion companion2 = CentralLog.Companion;
            String str2 = this.TAG;
            companion2.e(str2, "Unable to close receivers: " + th2.getLocalizedMessage());
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lau/gov/health/covidsafe/streetpass/StreetPassWorker$DeviceProcessedReceiver;", "Landroid/content/BroadcastReceiver;", "(Lau/gov/health/covidsafe/streetpass/StreetPassWorker;)V", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StreetPassWorker.kt */
    public final class DeviceProcessedReceiver extends BroadcastReceiver {
        public DeviceProcessedReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            Intrinsics.checkParameterIsNotNull(intent, "intent");
            if (Intrinsics.areEqual((Object) GATTKt.ACTION_DEVICE_PROCESSED, (Object) intent.getAction())) {
                String stringExtra = intent.getStringExtra(GATTKt.DEVICE_ADDRESS);
                CentralLog.Companion companion = CentralLog.Companion;
                String access$getTAG$p = StreetPassWorker.this.TAG;
                companion.d(access$getTAG$p, "Adding to blacklist: " + stringExtra);
                BlacklistEntry blacklistEntry = new BlacklistEntry(stringExtra);
                StreetPassWorker.this.blacklist.add(blacklistEntry);
                StreetPassWorker.access$getBlacklistHandler$p(StreetPassWorker.this).postDelayed(new StreetPassWorker$DeviceProcessedReceiver$onReceive$1(this, blacklistEntry), 100000);
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001c\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lau/gov/health/covidsafe/streetpass/StreetPassWorker$StreetPassWorkReceiver;", "Landroid/content/BroadcastReceiver;", "(Lau/gov/health/covidsafe/streetpass/StreetPassWorker;)V", "TAG", "", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StreetPassWorker.kt */
    public final class StreetPassWorkReceiver extends BroadcastReceiver {
        private final String TAG = "StreetPassWorkReceiver";

        public StreetPassWorkReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent != null && Intrinsics.areEqual((Object) StreetPassKt.ACTION_DEVICE_SCANNED, (Object) intent.getAction())) {
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                ConnectablePeripheral connectablePeripheral = (ConnectablePeripheral) intent.getParcelableExtra(GATTKt.CONNECTION_DATA);
                boolean z = true;
                boolean z2 = bluetoothDevice != null;
                if (connectablePeripheral == null) {
                    z = false;
                }
                CentralLog.Companion companion = CentralLog.Companion;
                String str = this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Device received: ");
                sb.append(bluetoothDevice != null ? bluetoothDevice.getAddress() : null);
                sb.append(". Device present: ");
                sb.append(z2);
                sb.append(", Connectable Present: ");
                sb.append(z);
                companion.i(str, sb.toString());
                if (bluetoothDevice != null && connectablePeripheral != null) {
                    if (StreetPassWorker.this.addWork(new Work(bluetoothDevice, connectablePeripheral, StreetPassWorker.this.getOnWorkTimeoutListener()))) {
                        StreetPassWorker.this.doWork();
                    }
                }
            }
        }
    }
}
