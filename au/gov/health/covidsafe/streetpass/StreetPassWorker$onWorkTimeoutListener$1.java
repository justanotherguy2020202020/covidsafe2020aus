package au.gov.health.covidsafe.streetpass;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import au.gov.health.covidsafe.logging.CentralLog;
import au.gov.health.covidsafe.streetpass.Work;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016¨\u0006\u0006"}, d2 = {"au/gov/health/covidsafe/streetpass/StreetPassWorker$onWorkTimeoutListener$1", "Lau/gov/health/covidsafe/streetpass/Work$OnWorkTimeoutListener;", "onWorkTimeout", "", "work", "Lau/gov/health/covidsafe/streetpass/Work;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StreetPassWorker.kt */
public final class StreetPassWorker$onWorkTimeoutListener$1 implements Work.OnWorkTimeoutListener {
    final /* synthetic */ StreetPassWorker this$0;

    StreetPassWorker$onWorkTimeoutListener$1(StreetPassWorker streetPassWorker) {
        this.this$0 = streetPassWorker;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x00e9, code lost:
        r3 = r3.getDevice();
     */
    public void onWorkTimeout(Work work) {
        BluetoothDevice device;
        Intrinsics.checkParameterIsNotNull(work, "work");
        if (!this.this$0.isCurrentlyWorkedOn(work.getDevice().getAddress())) {
            CentralLog.Companion.i(this.this$0.TAG, "Work already removed. Timeout ineffective??.");
        }
        CentralLog.Companion companion = CentralLog.Companion;
        String access$getTAG$p = this.this$0.TAG;
        companion.e(access$getTAG$p, "Work timed out for " + work.getDevice().getAddress() + " @ " + work.getConnectable().getRssi() + " queued for " + (work.getChecklist().getStarted().getTimePerformed() - work.getTimeStamp()) + "ms");
        CentralLog.Companion companion2 = CentralLog.Companion;
        String access$getTAG$p2 = this.this$0.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append(work.getDevice().getAddress());
        sb.append(" work status: ");
        sb.append(work.getChecklist());
        sb.append('.');
        companion2.e(access$getTAG$p2, sb.toString());
        if (!work.getChecklist().getConnected().getStatus()) {
            CentralLog.Companion companion3 = CentralLog.Companion;
            String access$getTAG$p3 = this.this$0.TAG;
            companion3.e(access$getTAG$p3, "No connection formed for " + work.getDevice().getAddress());
            String address = work.getDevice().getAddress();
            Work access$getCurrentPendingConnection$p = this.this$0.currentPendingConnection;
            String address2 = (access$getCurrentPendingConnection$p == null || device == null) ? null : device.getAddress();
            if (Intrinsics.areEqual((Object) address, (Object) address2)) {
                this.this$0.currentPendingConnection = null;
            }
            try {
                BluetoothGatt gatt = work.getGatt();
                if (gatt != null) {
                    gatt.close();
                }
            } catch (Exception e) {
                CentralLog.Companion companion4 = CentralLog.Companion;
                String access$getTAG$p4 = this.this$0.TAG;
                companion4.e(access$getTAG$p4, "Unexpected error while attempting to close clientIf to " + work.getDevice().getAddress() + ": " + e.getLocalizedMessage());
            }
            this.this$0.finishWork(work);
        } else if (!work.getChecklist().getConnected().getStatus() || work.getChecklist().getDisconnected().getStatus()) {
            CentralLog.Companion companion5 = CentralLog.Companion;
            String access$getTAG$p5 = this.this$0.TAG;
            companion5.e(access$getTAG$p5, "Disconnected but callback not invoked in time. Waiting.: " + work.getDevice().getAddress() + ": " + work.getChecklist());
        } else if (work.getChecklist().getReadCharacteristic().getStatus() || work.getChecklist().getWriteCharacteristic().getStatus() || work.getChecklist().getSkipped().getStatus()) {
            CentralLog.Companion companion6 = CentralLog.Companion;
            String access$getTAG$p6 = this.this$0.TAG;
            companion6.e(access$getTAG$p6, "Connected but did not disconnect in time for " + work.getDevice().getAddress());
            try {
                BluetoothGatt gatt2 = work.getGatt();
                if (gatt2 != null) {
                    gatt2.disconnect();
                }
                if (work.getGatt() == null) {
                    this.this$0.currentPendingConnection = null;
                    this.this$0.finishWork(work);
                }
            } catch (Throwable th) {
                CentralLog.Companion companion7 = CentralLog.Companion;
                String access$getTAG$p7 = this.this$0.TAG;
                companion7.e(access$getTAG$p7, "Failed to clean up work, bluetooth state likely changed or other device's advertiser stopped: " + th.getLocalizedMessage());
            }
        } else {
            CentralLog.Companion companion8 = CentralLog.Companion;
            String access$getTAG$p8 = this.this$0.TAG;
            companion8.e(access$getTAG$p8, "Connected but did nothing for " + work.getDevice().getAddress());
            try {
                BluetoothGatt gatt3 = work.getGatt();
                if (gatt3 != null) {
                    gatt3.disconnect();
                }
                if (work.getGatt() == null) {
                    this.this$0.currentPendingConnection = null;
                    this.this$0.finishWork(work);
                }
            } catch (Throwable th2) {
                CentralLog.Companion companion9 = CentralLog.Companion;
                String access$getTAG$p9 = this.this$0.TAG;
                companion9.e(access$getTAG$p9, "Failed to clean up work, bluetooth state likely changed or other device's advertiser stopped: " + th2.getLocalizedMessage());
            }
        }
    }
}
