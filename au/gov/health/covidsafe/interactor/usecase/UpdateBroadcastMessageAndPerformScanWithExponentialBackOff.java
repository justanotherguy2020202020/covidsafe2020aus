package au.gov.health.covidsafe.interactor.usecase;

import android.content.Context;
import androidx.lifecycle.Lifecycle;
import au.gov.health.covidsafe.Preference;
import au.gov.health.covidsafe.Utils;
import au.gov.health.covidsafe.interactor.Either;
import au.gov.health.covidsafe.interactor.Failure;
import au.gov.health.covidsafe.interactor.Success;
import au.gov.health.covidsafe.interactor.UseCase;
import au.gov.health.covidsafe.networking.response.BroadcastMessageResponse;
import au.gov.health.covidsafe.networking.service.AwsClient;
import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;
import kotlinx.coroutines.DelayKt;
import retrofit2.Response;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0001B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0018\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u000eH\u0002J+\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0014j\u0002`\u0015\u0012\u0004\u0012\u00020\u00020\u00132\b\u0010\u0016\u001a\u0004\u0018\u00010\u0003H@ø\u0001\u0000¢\u0006\u0002\u0010\u0017R\u000e\u0010\u000b\u001a\u00020\fXD¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0018"}, d2 = {"Lau/gov/health/covidsafe/interactor/usecase/UpdateBroadcastMessageAndPerformScanWithExponentialBackOff;", "Lau/gov/health/covidsafe/interactor/UseCase;", "Lau/gov/health/covidsafe/networking/response/BroadcastMessageResponse;", "Ljava/lang/Void;", "awsClient", "Lau/gov/health/covidsafe/networking/service/AwsClient;", "context", "Landroid/content/Context;", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "(Lau/gov/health/covidsafe/networking/service/AwsClient;Landroid/content/Context;Landroidx/lifecycle/Lifecycle;)V", "RETRIES_LIMIT", "", "TAG", "", "call", "Lretrofit2/Response;", "jwtToken", "run", "Lau/gov/health/covidsafe/interactor/Either;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "params", "(Ljava/lang/Void;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: UpdateBroadcastMessageAndPerformScanWithExponentialBackOff.kt */
public final class UpdateBroadcastMessageAndPerformScanWithExponentialBackOff extends UseCase<BroadcastMessageResponse, Void> {
    private final int RETRIES_LIMIT = 3;
    private final String TAG;
    private final AwsClient awsClient;
    private final Context context;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateBroadcastMessageAndPerformScanWithExponentialBackOff(AwsClient awsClient2, Context context2, Lifecycle lifecycle) {
        super(lifecycle);
        Intrinsics.checkParameterIsNotNull(awsClient2, "awsClient");
        Intrinsics.checkParameterIsNotNull(context2, "context");
        Intrinsics.checkParameterIsNotNull(lifecycle, "lifecycle");
        this.awsClient = awsClient2;
        this.context = context2;
        String simpleName = getClass().getSimpleName();
        Intrinsics.checkExpressionValueIsNotNull(simpleName, "this.javaClass.simpleName");
        this.TAG = simpleName;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00e0, code lost:
        r2 = kotlin.text.StringsKt.toLongOrNull(r2);
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    public Object run(Void voidR, Continuation<? super Either<? extends Exception, BroadcastMessageResponse>> continuation) {
        UpdateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1 updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1;
        Object coroutine_suspended;
        int i;
        UpdateBroadcastMessageAndPerformScanWithExponentialBackOff updateBroadcastMessageAndPerformScanWithExponentialBackOff;
        String str;
        String str2;
        int i2;
        Void voidR2;
        int i3;
        Response<BroadcastMessageResponse> call;
        Long longOrNull;
        Long longOrNull2;
        long pow;
        Continuation<? super Either<? extends Exception, BroadcastMessageResponse>> continuation2 = continuation;
        if (continuation2 instanceof UpdateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1) {
            updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1 = (UpdateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1) continuation2;
            if ((updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.label & Integer.MIN_VALUE) != 0) {
                updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.label -= Integer.MIN_VALUE;
                Object obj = updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.result;
                coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
                i = updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.label;
                boolean z = false;
                if (i != 0) {
                    ResultKt.throwOnFailure(obj);
                    String encrypterJWTToken = Preference.INSTANCE.getEncrypterJWTToken(this.context);
                    if (encrypterJWTToken != null) {
                        call = call(encrypterJWTToken);
                        updateBroadcastMessageAndPerformScanWithExponentialBackOff = this;
                        str2 = encrypterJWTToken;
                        str = str2;
                        i3 = 0;
                        voidR2 = voidR;
                    } else {
                        UpdateBroadcastMessageAndPerformScanWithExponentialBackOff updateBroadcastMessageAndPerformScanWithExponentialBackOff2 = this;
                        return new Failure(new Exception());
                    }
                } else if (i == 1) {
                    long j = updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.J$0;
                    i2 = updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.I$0;
                    Response response = (Response) updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$4;
                    updateBroadcastMessageAndPerformScanWithExponentialBackOff = (UpdateBroadcastMessageAndPerformScanWithExponentialBackOff) updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$0;
                    ResultKt.throwOnFailure(obj);
                    voidR2 = (Void) updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$1;
                    str = (String) updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$2;
                    str2 = (String) updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$3;
                    i3 = i2 + 1;
                    call = updateBroadcastMessageAndPerformScanWithExponentialBackOff.call(str2);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if ((call == null || !call.isSuccessful() || call.body() == null) && i3 >= updateBroadcastMessageAndPerformScanWithExponentialBackOff.RETRIES_LIMIT) {
                    pow = ((long) Math.pow((double) 2, (double) i3)) * ((long) 1000);
                    updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$0 = updateBroadcastMessageAndPerformScanWithExponentialBackOff;
                    updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$1 = voidR2;
                    updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$2 = str;
                    updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$3 = str2;
                    updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$4 = call;
                    updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.I$0 = i3;
                    updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.J$0 = pow;
                    updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.label = 1;
                    if (DelayKt.delay(pow, updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1) == coroutine_suspended) {
                        return coroutine_suspended;
                    }
                    i2 = i3;
                    i3 = i2 + 1;
                    call = updateBroadcastMessageAndPerformScanWithExponentialBackOff.call(str2);
                    if ((call == null && !call.isSuccessful() && call.body() == null) || i3 >= updateBroadcastMessageAndPerformScanWithExponentialBackOff.RETRIES_LIMIT) {
                        if (call == null || !call.isSuccessful()) {
                            return new Failure(new Exception());
                        }
                        BroadcastMessageResponse body = call.body();
                        if (body != null) {
                            CharSequence tempId = body.getTempId();
                            if (tempId == null || tempId.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                return new Failure(new Exception());
                            }
                            String expiryTime = body.getExpiryTime();
                            long j2 = 0;
                            long longValue = (expiryTime == null || longOrNull2 == null) ? 0 : longOrNull2.longValue();
                            long j3 = (long) 1000;
                            Preference.INSTANCE.putExpiryTimeInMillis(updateBroadcastMessageAndPerformScanWithExponentialBackOff.context, longValue * j3);
                            String refreshTime = body.getRefreshTime();
                            if (!(refreshTime == null || (longOrNull = StringsKt.toLongOrNull(refreshTime)) == null)) {
                                j2 = longOrNull.longValue();
                            }
                            Preference.INSTANCE.putNextFetchTimeInMillis(updateBroadcastMessageAndPerformScanWithExponentialBackOff.context, j2 * j3);
                            Utils.INSTANCE.storeBroadcastMessage(updateBroadcastMessageAndPerformScanWithExponentialBackOff.context, body.getTempId());
                            return new Success(body);
                        }
                        UpdateBroadcastMessageAndPerformScanWithExponentialBackOff updateBroadcastMessageAndPerformScanWithExponentialBackOff3 = updateBroadcastMessageAndPerformScanWithExponentialBackOff;
                        return new Failure(new Exception());
                    }
                    return coroutine_suspended;
                }
                pow = ((long) Math.pow((double) 2, (double) i3)) * ((long) 1000);
                updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$0 = updateBroadcastMessageAndPerformScanWithExponentialBackOff;
                updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$1 = voidR2;
                updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$2 = str;
                updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$3 = str2;
                updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$4 = call;
                updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.I$0 = i3;
                updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.J$0 = pow;
                updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.label = 1;
                if (DelayKt.delay(pow, updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1) == coroutine_suspended) {
                }
                return coroutine_suspended;
            }
        }
        updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1 = new UpdateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1(this, continuation2);
        Object obj2 = updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.result;
        coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        i = updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.label;
        boolean z2 = false;
        if (i != 0) {
        }
        if (!(call == null && !call.isSuccessful() && call.body() == null) && i3 >= updateBroadcastMessageAndPerformScanWithExponentialBackOff.RETRIES_LIMIT) {
        }
        pow = ((long) Math.pow((double) 2, (double) i3)) * ((long) 1000);
        updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$0 = updateBroadcastMessageAndPerformScanWithExponentialBackOff;
        updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$1 = voidR2;
        updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$2 = str;
        updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$3 = str2;
        updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.L$4 = call;
        updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.I$0 = i3;
        updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.J$0 = pow;
        updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1.label = 1;
        if (DelayKt.delay(pow, updateBroadcastMessageAndPerformScanWithExponentialBackOff$run$1) == coroutine_suspended) {
        }
        return coroutine_suspended;
    }

    private final Response<BroadcastMessageResponse> call(String str) {
        try {
            AwsClient awsClient2 = this.awsClient;
            return awsClient2.getTempId("Bearer " + str).execute();
        } catch (Exception unused) {
            return null;
        }
    }
}
