package au.gov.health.covidsafe.interactor.usecase;

import android.content.Context;
import androidx.lifecycle.Lifecycle;
import au.gov.health.covidsafe.Preference;
import au.gov.health.covidsafe.TracerApp;
import au.gov.health.covidsafe.interactor.Either;
import au.gov.health.covidsafe.interactor.Failure;
import au.gov.health.covidsafe.interactor.Success;
import au.gov.health.covidsafe.interactor.UseCase;
import au.gov.health.covidsafe.interactor.usecase.UploadDataException;
import au.gov.health.covidsafe.logging.CentralLog;
import au.gov.health.covidsafe.networking.response.InitiateUploadResponse;
import au.gov.health.covidsafe.networking.service.AwsClient;
import au.gov.health.covidsafe.streetpass.persistence.StreetPassRecordStorage;
import au.gov.health.covidsafe.ui.upload.model.ExportData;
import com.google.gson.Gson;
import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Response;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u0001B'\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ)\u0010\r\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u0004\u0012\u00020\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H@ø\u0001\u0000¢\u0006\u0002\u0010\u0012J)\u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u0004\u0012\u00020\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u0002H@ø\u0001\u0000¢\u0006\u0002\u0010\u0012R\u000e\u0010\f\u001a\u00020\u0002X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0015"}, d2 = {"Lau/gov/health/covidsafe/interactor/usecase/UploadData;", "Lau/gov/health/covidsafe/interactor/UseCase;", "", "awsClient", "Lau/gov/health/covidsafe/networking/service/AwsClient;", "okHttpClient", "Lokhttp3/OkHttpClient;", "context", "Landroid/content/Context;", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "(Lau/gov/health/covidsafe/networking/service/AwsClient;Lokhttp3/OkHttpClient;Landroid/content/Context;Landroidx/lifecycle/Lifecycle;)V", "TAG", "run", "Lau/gov/health/covidsafe/interactor/Either;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "params", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "zipAndUploadData", "uploadLink", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: UploadData.kt */
public final class UploadData extends UseCase<String, String> {
    private final String TAG;
    /* access modifiers changed from: private */
    public final AwsClient awsClient;
    private final Context context;
    /* access modifiers changed from: private */
    public final OkHttpClient okHttpClient;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UploadData(AwsClient awsClient2, OkHttpClient okHttpClient2, Context context2, Lifecycle lifecycle) {
        super(lifecycle);
        Intrinsics.checkParameterIsNotNull(awsClient2, "awsClient");
        Intrinsics.checkParameterIsNotNull(okHttpClient2, "okHttpClient");
        Intrinsics.checkParameterIsNotNull(lifecycle, "lifecycle");
        this.awsClient = awsClient2;
        this.okHttpClient = okHttpClient2;
        this.context = context2;
        String simpleName = getClass().getSimpleName();
        Intrinsics.checkExpressionValueIsNotNull(simpleName, "this.javaClass.simpleName");
        this.TAG = simpleName;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x009a A[Catch:{ Exception -> 0x0046 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00a5 A[Catch:{ Exception -> 0x0046 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0126 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public Object run(String str, Continuation<? super Either<? extends Exception, String>> continuation) {
        UploadData$run$1 uploadData$run$1;
        int i;
        UploadData uploadData;
        Either either;
        String str2;
        String str3;
        String str4;
        Object obj;
        Response response;
        Failure failure;
        if (continuation instanceof UploadData$run$1) {
            uploadData$run$1 = (UploadData$run$1) continuation;
            if ((uploadData$run$1.label & Integer.MIN_VALUE) != 0) {
                uploadData$run$1.label -= Integer.MIN_VALUE;
                Object obj2 = uploadData$run$1.result;
                Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
                i = uploadData$run$1.label;
                boolean z = true;
                if (i != 0) {
                    ResultKt.throwOnFailure(obj2);
                    str2 = Preference.INSTANCE.getEncrypterJWTToken(this.context);
                    if (str2 != null) {
                        try {
                            uploadData$run$1.L$0 = this;
                            uploadData$run$1.L$1 = str;
                            uploadData$run$1.L$2 = str2;
                            uploadData$run$1.L$3 = str2;
                            uploadData$run$1.label = 1;
                            obj = retryRetrofitCall(new UploadData$run$$inlined$let$lambda$1(str2, this, uploadData$run$1, str), uploadData$run$1);
                            if (obj == coroutine_suspended) {
                                return coroutine_suspended;
                            }
                            str4 = str;
                            str3 = str2;
                            uploadData = this;
                        } catch (Exception e) {
                            e = e;
                            uploadData = this;
                            either = new Failure(e);
                            if (either != null) {
                            }
                            UploadData uploadData2 = uploadData;
                            return new Failure(new Exception());
                        }
                    } else {
                        uploadData = this;
                        UploadData uploadData22 = uploadData;
                        return new Failure(new Exception());
                    }
                } else if (i == 1) {
                    String str5 = (String) uploadData$run$1.L$3;
                    String str6 = (String) uploadData$run$1.L$2;
                    str4 = (String) uploadData$run$1.L$1;
                    UploadData uploadData3 = (UploadData) uploadData$run$1.L$0;
                    try {
                        ResultKt.throwOnFailure(obj2);
                        Object obj3 = obj2;
                        str2 = str5;
                        uploadData = uploadData3;
                        str3 = str6;
                        obj = obj3;
                    } catch (Exception e2) {
                        e = e2;
                        uploadData = uploadData3;
                        either = new Failure(e);
                        if (either != null) {
                        }
                        UploadData uploadData222 = uploadData;
                        return new Failure(new Exception());
                    }
                } else if (i == 2) {
                    String str7 = (String) uploadData$run$1.L$5;
                    Response response2 = (Response) uploadData$run$1.L$4;
                    String str8 = (String) uploadData$run$1.L$3;
                    String str9 = (String) uploadData$run$1.L$2;
                    String str10 = (String) uploadData$run$1.L$1;
                    uploadData = (UploadData) uploadData$run$1.L$0;
                    try {
                        ResultKt.throwOnFailure(obj2);
                        either = (Either) obj2;
                    } catch (Exception e3) {
                        e = e3;
                    }
                    if (either != null) {
                        return either;
                    }
                    UploadData uploadData2222 = uploadData;
                    return new Failure(new Exception());
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                response = (Response) obj;
                if (response != null) {
                    either = new Failure(UploadDataException.UploadDataIncorrectPinException.INSTANCE);
                } else if (response.isSuccessful()) {
                    InitiateUploadResponse initiateUploadResponse = (InitiateUploadResponse) response.body();
                    String uploadLink = initiateUploadResponse != null ? initiateUploadResponse.getUploadLink() : null;
                    CharSequence charSequence = uploadLink;
                    if (charSequence != null) {
                        if (charSequence.length() != 0) {
                            z = false;
                        }
                    }
                    if (z) {
                        either = new Failure(new Exception());
                    } else {
                        uploadData$run$1.L$0 = uploadData;
                        uploadData$run$1.L$1 = str4;
                        uploadData$run$1.L$2 = str3;
                        uploadData$run$1.L$3 = str2;
                        uploadData$run$1.L$4 = response;
                        uploadData$run$1.L$5 = uploadLink;
                        uploadData$run$1.label = 2;
                        obj2 = uploadData.zipAndUploadData(uploadLink, uploadData$run$1);
                        if (obj2 == coroutine_suspended) {
                            return coroutine_suspended;
                        }
                        either = (Either) obj2;
                    }
                } else {
                    if (response.code() == 400) {
                        failure = new Failure(UploadDataException.UploadDataIncorrectPinException.INSTANCE);
                    } else if (response.code() == 403) {
                        failure = new Failure(UploadDataException.UploadDataJwtExpiredException.INSTANCE);
                    } else {
                        failure = new Failure(new Exception());
                    }
                    either = failure;
                }
                if (either != null) {
                }
                UploadData uploadData22222 = uploadData;
                return new Failure(new Exception());
            }
        }
        uploadData$run$1 = new UploadData$run$1(this, continuation);
        Object obj22 = uploadData$run$1.result;
        Object coroutine_suspended2 = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        i = uploadData$run$1.label;
        boolean z2 = true;
        if (i != 0) {
        }
        response = (Response) obj;
        if (response != null) {
        }
        if (either != null) {
        }
        UploadData uploadData222222 = uploadData;
        return new Failure(new Exception());
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00bd A[Catch:{ Exception -> 0x00d3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ca A[Catch:{ Exception -> 0x00d3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final /* synthetic */ Object zipAndUploadData(String str, Continuation<? super Either<? extends Exception, String>> continuation) {
        UploadData$zipAndUploadData$1 uploadData$zipAndUploadData$1;
        Object obj;
        int i;
        if (continuation instanceof UploadData$zipAndUploadData$1) {
            uploadData$zipAndUploadData$1 = (UploadData$zipAndUploadData$1) continuation;
            if ((uploadData$zipAndUploadData$1.label & Integer.MIN_VALUE) != 0) {
                uploadData$zipAndUploadData$1.label -= Integer.MIN_VALUE;
                obj = uploadData$zipAndUploadData$1.result;
                Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
                i = uploadData$zipAndUploadData$1.label;
                if (i != 0) {
                    ResultKt.throwOnFailure(obj);
                    ExportData exportData = new ExportData(new StreetPassRecordStorage(TracerApp.Companion.getAppContext()).getAllRecords());
                    CentralLog.Companion.d(this.TAG, "records: " + exportData.getRecords());
                    String json = new Gson().toJson((Object) exportData);
                    Request.Builder url = new Request.Builder().url(str);
                    RequestBody.Companion companion = RequestBody.Companion;
                    Intrinsics.checkExpressionValueIsNotNull(json, "jsonData");
                    Request build = url.put(companion.create(json, (MediaType) null)).build();
                    uploadData$zipAndUploadData$1.L$0 = this;
                    uploadData$zipAndUploadData$1.L$1 = str;
                    uploadData$zipAndUploadData$1.L$2 = exportData;
                    uploadData$zipAndUploadData$1.L$3 = json;
                    uploadData$zipAndUploadData$1.L$4 = build;
                    uploadData$zipAndUploadData$1.label = 1;
                    obj = retryOkhttpCall(new UploadData$zipAndUploadData$response$1(this, build), uploadData$zipAndUploadData$1);
                    if (obj == coroutine_suspended) {
                        return coroutine_suspended;
                    }
                } else if (i == 1) {
                    Request request = (Request) uploadData$zipAndUploadData$1.L$4;
                    String str2 = (String) uploadData$zipAndUploadData$1.L$3;
                    ExportData exportData2 = (ExportData) uploadData$zipAndUploadData$1.L$2;
                    str = (String) uploadData$zipAndUploadData$1.L$1;
                    UploadData uploadData = (UploadData) uploadData$zipAndUploadData$1.L$0;
                    try {
                        ResultKt.throwOnFailure(obj);
                    } catch (Exception unused) {
                        return new Failure(new Exception());
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (((okhttp3.Response) obj) != null) {
                    return new Failure(new Exception());
                }
                return new Success(str);
            }
        }
        uploadData$zipAndUploadData$1 = new UploadData$zipAndUploadData$1(this, continuation);
        obj = uploadData$zipAndUploadData$1.result;
        Object coroutine_suspended2 = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        i = uploadData$zipAndUploadData$1.label;
        if (i != 0) {
        }
        if (((okhttp3.Response) obj) != null) {
        }
    }
}
