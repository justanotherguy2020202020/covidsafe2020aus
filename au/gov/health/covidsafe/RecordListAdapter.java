package au.gov.health.covidsafe;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord;
import au.gov.health.covidsafe.streetpass.view.StreetPassRecordViewModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0002%&B\u000f\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0018\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\b\u0010\u0010\u001a\u0004\u0018\u00010\fH\u0002J&\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\b\u0010\u0012\u001a\u0004\u0018\u00010\f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\u000bH\u0002J&\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\b\u0010\u0012\u001a\u0004\u0018\u00010\f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\u000bH\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0016J\u001c\u0010\u0017\u001a\u00020\u00182\n\u0010\u0019\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001a\u001a\u00020\u0016H\u0016J\u001c\u0010\u001b\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0016H\u0016J\u001c\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\u000bH\u0002J\u001c\u0010 \u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\u000bH\u0002J\u000e\u0010!\u001a\u00020\u00182\u0006\u0010\b\u001a\u00020\tJ\u001a\u0010!\u001a\u00020\u00182\u0006\u0010\b\u001a\u00020\t2\b\u0010\u0012\u001a\u0004\u0018\u00010\fH\u0002J\u0016\u0010\"\u001a\u00020\u00182\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH\u0002J\u001b\u0010#\u001a\u00020\u00182\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000e0\u000bH\u0000¢\u0006\u0002\b$R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u000bX\u000e¢\u0006\u0002\n\u0000¨\u0006'"}, d2 = {"Lau/gov/health/covidsafe/RecordListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lau/gov/health/covidsafe/RecordListAdapter$RecordViewHolder;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "inflater", "Landroid/view/LayoutInflater;", "mode", "Lau/gov/health/covidsafe/RecordListAdapter$MODE;", "records", "", "Lau/gov/health/covidsafe/streetpass/view/StreetPassRecordViewModel;", "sourceData", "Lau/gov/health/covidsafe/streetpass/persistence/StreetPassRecord;", "filter", "sample", "filterByModelC", "model", "words", "filterByModelP", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "prepareCollapsedData", "prepareViewData", "setMode", "setRecords", "setSourceData", "setSourceData$app_release", "MODE", "RecordViewHolder", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RecordListAdapter.kt */
public final class RecordListAdapter extends RecyclerView.Adapter<RecordViewHolder> {
    private final LayoutInflater inflater;
    private MODE mode = MODE.ALL;
    private List<StreetPassRecordViewModel> records = CollectionsKt.emptyList();
    private List<StreetPassRecord> sourceData = CollectionsKt.emptyList();

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lau/gov/health/covidsafe/RecordListAdapter$MODE;", "", "(Ljava/lang/String;I)V", "ALL", "COLLAPSE", "MODEL_P", "MODEL_C", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: RecordListAdapter.kt */
    public enum MODE {
        ALL,
        COLLAPSE,
        MODEL_P,
        MODEL_C
    }

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[MODE.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[MODE.COLLAPSE.ordinal()] = 1;
            $EnumSwitchMapping$0[MODE.ALL.ordinal()] = 2;
            $EnumSwitchMapping$0[MODE.MODEL_P.ordinal()] = 3;
            $EnumSwitchMapping$0[MODE.MODEL_C.ordinal()] = 4;
        }
    }

    public RecordListAdapter(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        LayoutInflater from = LayoutInflater.from(context);
        Intrinsics.checkExpressionValueIsNotNull(from, "LayoutInflater.from(context)");
        this.inflater = from;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0013\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0007R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0010\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\rR\u0011\u0010\u0012\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\rR\u0011\u0010\u0014\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\rR\u0011\u0010\u0016\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\rR\u0011\u0010\u0018\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\rR\u0011\u0010\u001a\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\rR\u0011\u0010\u001c\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\r¨\u0006\u001e"}, d2 = {"Lau/gov/health/covidsafe/RecordListAdapter$RecordViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Lau/gov/health/covidsafe/RecordListAdapter;Landroid/view/View;)V", "filterModelC", "getFilterModelC", "()Landroid/view/View;", "filterModelP", "getFilterModelP", "findsView", "Landroid/widget/TextView;", "getFindsView", "()Landroid/widget/TextView;", "modelCView", "getModelCView", "modelPView", "getModelPView", "msgView", "getMsgView", "org", "getOrg", "signalStrengthView", "getSignalStrengthView", "timestampView", "getTimestampView", "txpowerView", "getTxpowerView", "version", "getVersion", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: RecordListAdapter.kt */
    public final class RecordViewHolder extends RecyclerView.ViewHolder {
        private final View filterModelC;
        private final View filterModelP;
        private final TextView findsView;
        private final TextView modelCView;
        private final TextView modelPView;
        private final TextView msgView;

        /* renamed from: org  reason: collision with root package name */
        private final TextView f4org;
        private final TextView signalStrengthView;
        final /* synthetic */ RecordListAdapter this$0;
        private final TextView timestampView;
        private final TextView txpowerView;
        private final TextView version;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public RecordViewHolder(RecordListAdapter recordListAdapter, View view) {
            super(view);
            Intrinsics.checkParameterIsNotNull(view, "itemView");
            this.this$0 = recordListAdapter;
            View findViewById = view.findViewById(R.id.modelc);
            Intrinsics.checkExpressionValueIsNotNull(findViewById, "itemView.findViewById(R.id.modelc)");
            this.modelCView = (TextView) findViewById;
            View findViewById2 = view.findViewById(R.id.modelp);
            Intrinsics.checkExpressionValueIsNotNull(findViewById2, "itemView.findViewById(R.id.modelp)");
            this.modelPView = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.timestamp);
            Intrinsics.checkExpressionValueIsNotNull(findViewById3, "itemView.findViewById(R.id.timestamp)");
            this.timestampView = (TextView) findViewById3;
            View findViewById4 = view.findViewById(R.id.finds);
            Intrinsics.checkExpressionValueIsNotNull(findViewById4, "itemView.findViewById(R.id.finds)");
            this.findsView = (TextView) findViewById4;
            View findViewById5 = view.findViewById(R.id.txpower);
            Intrinsics.checkExpressionValueIsNotNull(findViewById5, "itemView.findViewById(R.id.txpower)");
            this.txpowerView = (TextView) findViewById5;
            View findViewById6 = view.findViewById(R.id.signal_strength);
            Intrinsics.checkExpressionValueIsNotNull(findViewById6, "itemView.findViewById(R.id.signal_strength)");
            this.signalStrengthView = (TextView) findViewById6;
            View findViewById7 = view.findViewById(R.id.filter_by_modelp);
            Intrinsics.checkExpressionValueIsNotNull(findViewById7, "itemView.findViewById(R.id.filter_by_modelp)");
            this.filterModelP = findViewById7;
            View findViewById8 = view.findViewById(R.id.filter_by_modelc);
            Intrinsics.checkExpressionValueIsNotNull(findViewById8, "itemView.findViewById(R.id.filter_by_modelc)");
            this.filterModelC = findViewById8;
            View findViewById9 = view.findViewById(R.id.msg);
            Intrinsics.checkExpressionValueIsNotNull(findViewById9, "itemView.findViewById(R.id.msg)");
            this.msgView = (TextView) findViewById9;
            View findViewById10 = view.findViewById(R.id.version);
            Intrinsics.checkExpressionValueIsNotNull(findViewById10, "itemView.findViewById(R.id.version)");
            this.version = (TextView) findViewById10;
            View findViewById11 = view.findViewById(R.id.f3org);
            Intrinsics.checkExpressionValueIsNotNull(findViewById11, "itemView.findViewById(R.id.org)");
            this.f4org = (TextView) findViewById11;
        }

        public final TextView getModelCView() {
            return this.modelCView;
        }

        public final TextView getModelPView() {
            return this.modelPView;
        }

        public final TextView getTimestampView() {
            return this.timestampView;
        }

        public final TextView getFindsView() {
            return this.findsView;
        }

        public final TextView getTxpowerView() {
            return this.txpowerView;
        }

        public final TextView getSignalStrengthView() {
            return this.signalStrengthView;
        }

        public final View getFilterModelP() {
            return this.filterModelP;
        }

        public final View getFilterModelC() {
            return this.filterModelC;
        }

        public final TextView getMsgView() {
            return this.msgView;
        }

        public final TextView getVersion() {
            return this.version;
        }

        public final TextView getOrg() {
            return this.f4org;
        }
    }

    public RecordViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        View inflate = this.inflater.inflate(R.layout.recycler_view_item, viewGroup, false);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "itemView");
        return new RecordViewHolder(this, inflate);
    }

    public void onBindViewHolder(RecordViewHolder recordViewHolder, int i) {
        Intrinsics.checkParameterIsNotNull(recordViewHolder, "holder");
        StreetPassRecordViewModel streetPassRecordViewModel = this.records.get(i);
        recordViewHolder.getMsgView().setText(streetPassRecordViewModel.getMsg());
        recordViewHolder.getModelCView().setText(streetPassRecordViewModel.getModelC());
        recordViewHolder.getModelPView().setText(streetPassRecordViewModel.getModelP());
        TextView findsView = recordViewHolder.getFindsView();
        findsView.setText("Detections: " + streetPassRecordViewModel.getNumber());
        recordViewHolder.getTimestampView().setText(Utils.INSTANCE.getDate(streetPassRecordViewModel.getTimeStamp()));
        TextView version = recordViewHolder.getVersion();
        version.setText("v: " + streetPassRecordViewModel.getVersion());
        TextView org2 = recordViewHolder.getOrg();
        org2.setText("ORG: " + streetPassRecordViewModel.getOrg());
        recordViewHolder.getFilterModelP().setTag(streetPassRecordViewModel);
        recordViewHolder.getFilterModelC().setTag(streetPassRecordViewModel);
        TextView signalStrengthView = recordViewHolder.getSignalStrengthView();
        signalStrengthView.setText("Signal Strength: " + streetPassRecordViewModel.getRssi());
        TextView txpowerView = recordViewHolder.getTxpowerView();
        txpowerView.setText("Tx Power: " + streetPassRecordViewModel.getTransmissionPower());
        recordViewHolder.getFilterModelP().setOnClickListener(new RecordListAdapter$onBindViewHolder$1(this));
        recordViewHolder.getFilterModelC().setOnClickListener(new RecordListAdapter$onBindViewHolder$2(this));
    }

    private final List<StreetPassRecordViewModel> filter(StreetPassRecordViewModel streetPassRecordViewModel) {
        int i = WhenMappings.$EnumSwitchMapping$0[this.mode.ordinal()];
        if (i == 1) {
            return prepareCollapsedData(this.sourceData);
        }
        if (i == 2) {
            return prepareViewData(this.sourceData);
        }
        if (i == 3) {
            return filterByModelP(streetPassRecordViewModel, this.sourceData);
        }
        if (i != 4) {
            return prepareViewData(this.sourceData);
        }
        return filterByModelC(streetPassRecordViewModel, this.sourceData);
    }

    private final List<StreetPassRecordViewModel> filterByModelC(StreetPassRecordViewModel streetPassRecordViewModel, List<StreetPassRecord> list) {
        if (streetPassRecordViewModel == null) {
            return prepareViewData(list);
        }
        Collection arrayList = new ArrayList();
        for (Object next : list) {
            if (Intrinsics.areEqual((Object) ((StreetPassRecord) next).getModelC(), (Object) streetPassRecordViewModel.getModelC())) {
                arrayList.add(next);
            }
        }
        return prepareViewData((List) arrayList);
    }

    private final List<StreetPassRecordViewModel> filterByModelP(StreetPassRecordViewModel streetPassRecordViewModel, List<StreetPassRecord> list) {
        if (streetPassRecordViewModel == null) {
            return prepareViewData(list);
        }
        Collection arrayList = new ArrayList();
        for (Object next : list) {
            if (Intrinsics.areEqual((Object) ((StreetPassRecord) next).getModelP(), (Object) streetPassRecordViewModel.getModelP())) {
                arrayList.add(next);
            }
        }
        return prepareViewData((List) arrayList);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v3, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v0, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v0, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v18, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v19, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v20, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v21, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v22, resolved type: au.gov.health.covidsafe.streetpass.persistence.StreetPassRecord} */
    /* JADX WARNING: Multi-variable type inference failed */
    private final List<StreetPassRecordViewModel> prepareCollapsedData(List<StreetPassRecord> list) {
        StreetPassRecordViewModel streetPassRecordViewModel;
        StreetPassRecord streetPassRecord;
        Iterable iterable = list;
        Map linkedHashMap = new LinkedHashMap();
        for (Object next : iterable) {
            String modelC = ((StreetPassRecord) next).getModelC();
            Object obj = linkedHashMap.get(modelC);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(modelC, obj);
            }
            ((List) obj).add(next);
        }
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        for (Object next2 : iterable) {
            if (hashSet.add(((StreetPassRecord) next2).getModelC())) {
                arrayList.add(next2);
            }
        }
        Iterable<StreetPassRecord> iterable2 = arrayList;
        Collection arrayList2 = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable2, 10));
        for (StreetPassRecord streetPassRecord2 : iterable2) {
            List list2 = (List) linkedHashMap.get(streetPassRecord2.getModelC());
            StreetPassRecord streetPassRecord3 = null;
            Integer valueOf = list2 != null ? Integer.valueOf(list2.size()) : null;
            if (valueOf != null) {
                int intValue = valueOf.intValue();
                Iterable iterable3 = (List) linkedHashMap.get(streetPassRecord2.getModelC());
                if (iterable3 != null) {
                    Iterator it = iterable3.iterator();
                    if (it.hasNext()) {
                        Object next3 = it.next();
                        streetPassRecord3 = next3;
                        if (it.hasNext()) {
                            long timestamp = next3.getTimestamp();
                            StreetPassRecord streetPassRecord4 = next3;
                            do {
                                Object next4 = it.next();
                                long timestamp2 = next4.getTimestamp();
                                streetPassRecord = streetPassRecord4;
                                if (timestamp < timestamp2) {
                                    streetPassRecord = next4;
                                    timestamp = timestamp2;
                                }
                                streetPassRecord4 = streetPassRecord;
                            } while (it.hasNext());
                            streetPassRecord3 = streetPassRecord;
                        }
                    }
                    streetPassRecord3 = streetPassRecord3;
                }
                if (streetPassRecord3 != null) {
                    streetPassRecordViewModel = new StreetPassRecordViewModel(streetPassRecord3, intValue);
                } else {
                    streetPassRecordViewModel = new StreetPassRecordViewModel(streetPassRecord2, intValue);
                }
            } else {
                streetPassRecordViewModel = new StreetPassRecordViewModel(streetPassRecord2);
            }
            arrayList2.add(streetPassRecordViewModel);
        }
        return (List) arrayList2;
    }

    private final List<StreetPassRecordViewModel> prepareViewData(List<StreetPassRecord> list) {
        Iterable<StreetPassRecord> reversed = CollectionsKt.reversed(list);
        Collection arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(reversed, 10));
        for (StreetPassRecord streetPassRecordViewModel : reversed) {
            arrayList.add(new StreetPassRecordViewModel(streetPassRecordViewModel));
        }
        return (List) arrayList;
    }

    public final void setMode(MODE mode2) {
        Intrinsics.checkParameterIsNotNull(mode2, "mode");
        setMode(mode2, (StreetPassRecordViewModel) null);
    }

    /* access modifiers changed from: private */
    public final void setMode(MODE mode2, StreetPassRecordViewModel streetPassRecordViewModel) {
        this.mode = mode2;
        setRecords(filter(streetPassRecordViewModel));
    }

    private final void setRecords(List<StreetPassRecordViewModel> list) {
        this.records = list;
        notifyDataSetChanged();
    }

    public final void setSourceData$app_release(List<StreetPassRecord> list) {
        Intrinsics.checkParameterIsNotNull(list, "records");
        this.sourceData = list;
        setMode(this.mode);
    }

    public int getItemCount() {
        return this.records.size();
    }
}
