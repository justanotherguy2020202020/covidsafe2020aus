package au.gov.health.covidsafe.ui.onboarding.fragment.personal;

import android.content.Context;
import au.gov.health.covidsafe.Preference;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0012\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\tH\u0002J$\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\t2\b\u0010\f\u001a\u0004\u0018\u00010\t2\b\u0010\u0010\u001a\u0004\u0018\u00010\tJ&\u0010\u0011\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\t2\b\u0010\f\u001a\u0004\u0018\u00010\t2\b\u0010\u0010\u001a\u0004\u0018\u00010\tH\u0002J\u0012\u0010\u0012\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\tH\u0002J\u0012\u0010\u0013\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\tH\u0002J\u0012\u0010\u0014\u001a\u00020\u000e2\b\u0010\f\u001a\u0004\u0018\u00010\tH\u0002J\u0017\u0010\u0015\u001a\u00020\u000e2\b\u0010\f\u001a\u0004\u0018\u00010\tH\u0000¢\u0006\u0002\b\u0016J$\u0010\u0017\u001a\u00020\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\t2\b\u0010\f\u001a\u0004\u0018\u00010\t2\b\u0010\u0010\u001a\u0004\u0018\u00010\tR\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lau/gov/health/covidsafe/ui/onboarding/fragment/personal/PersonalDetailsPresenter;", "", "personalDetailsFragment", "Lau/gov/health/covidsafe/ui/onboarding/fragment/personal/PersonalDetailsFragment;", "(Lau/gov/health/covidsafe/ui/onboarding/fragment/personal/PersonalDetailsFragment;)V", "POST_CODE_REGEX", "Ljava/util/regex/Pattern;", "kotlin.jvm.PlatformType", "TAG", "", "isPostCodeValid", "", "postCode", "saveInfos", "", "name", "age", "showFieldsError", "updateAgeFieldError", "updateNameFieldError", "updatePostcodeFieldError", "validateInlinePostCode", "validateInlinePostCode$app_release", "validateInputsForButtonUpdate", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PersonalDetailsPresenter.kt */
public final class PersonalDetailsPresenter {
    private final Pattern POST_CODE_REGEX = Pattern.compile("^(?:(?:[2-8]\\d|9[0-7]|0?[28]|0?9(?=09))(?:\\d{2}))$");
    private final String TAG;
    private final PersonalDetailsFragment personalDetailsFragment;

    public PersonalDetailsPresenter(PersonalDetailsFragment personalDetailsFragment2) {
        Intrinsics.checkParameterIsNotNull(personalDetailsFragment2, "personalDetailsFragment");
        this.personalDetailsFragment = personalDetailsFragment2;
        String simpleName = getClass().getSimpleName();
        Intrinsics.checkExpressionValueIsNotNull(simpleName, "this.javaClass.simpleName");
        this.TAG = simpleName;
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00a5  */
    public final void saveInfos(String str, String str2, String str3) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        this.personalDetailsFragment.showLoading();
        Context context = this.personalDetailsFragment.getContext();
        if (context != null) {
            Integer intOrNull = str3 != null ? StringsKt.toIntOrNull(str3) : null;
            CharSequence charSequence = str;
            boolean z5 = true;
            boolean z6 = false;
            boolean z7 = !(charSequence == null || StringsKt.isBlank(charSequence));
            CharSequence charSequence2 = str2;
            boolean z8 = !(charSequence2 == null || StringsKt.isBlank(charSequence2)) && isPostCodeValid(str2);
            CharSequence charSequence3 = str3;
            boolean z9 = !(charSequence3 == null || StringsKt.isBlank(charSequence3));
            if (!z7 || !z8 || !z9) {
                showFieldsError(str, str2, str3);
                this.personalDetailsFragment.hideLoading();
                return;
            }
            if (str != null) {
                Preference preference = Preference.INSTANCE;
                Intrinsics.checkExpressionValueIsNotNull(context, "context");
                z = preference.putName(context, str);
            } else {
                z = false;
            }
            if (z) {
                if (str3 != null) {
                    Preference preference2 = Preference.INSTANCE;
                    Intrinsics.checkExpressionValueIsNotNull(context, "context");
                    z3 = preference2.putAge(context, str3);
                } else {
                    z3 = false;
                }
                if (z3) {
                    if (str2 != null) {
                        Preference preference3 = Preference.INSTANCE;
                        Intrinsics.checkExpressionValueIsNotNull(context, "context");
                        z4 = preference3.putPostCode(context, str2);
                    } else {
                        z4 = false;
                    }
                    if (z4) {
                        z2 = true;
                        if (!z2) {
                            this.personalDetailsFragment.hideLoading();
                            PersonalDetailsFragment personalDetailsFragment2 = this.personalDetailsFragment;
                            if (intOrNull != null) {
                                if (intOrNull.intValue() >= 16) {
                                    z5 = false;
                                }
                                z6 = z5;
                            }
                            personalDetailsFragment2.navigateToNextPage(z6);
                            return;
                        }
                        this.personalDetailsFragment.hideLoading();
                        this.personalDetailsFragment.showGenericError();
                        return;
                    }
                }
            }
            z2 = false;
            if (!z2) {
            }
        } else {
            PersonalDetailsPresenter personalDetailsPresenter = this;
            personalDetailsPresenter.personalDetailsFragment.hideLoading();
            personalDetailsPresenter.personalDetailsFragment.showGenericError();
        }
    }

    private final void showFieldsError(String str, String str2, String str3) {
        updateNameFieldError(str);
        updateAgeFieldError(str3);
        updatePostcodeFieldError(str2);
    }

    private final void updateAgeFieldError(String str) {
        CharSequence charSequence = str;
        if (charSequence == null || StringsKt.isBlank(charSequence)) {
            this.personalDetailsFragment.showAgeError();
        } else {
            this.personalDetailsFragment.hideAgeError();
        }
    }

    private final void updateNameFieldError(String str) {
        CharSequence charSequence = str;
        if (charSequence == null || StringsKt.isBlank(charSequence)) {
            this.personalDetailsFragment.showNameError();
        } else {
            this.personalDetailsFragment.hideNameError();
        }
    }

    private final void updatePostcodeFieldError(String str) {
        CharSequence charSequence = str;
        if (charSequence == null || StringsKt.isBlank(charSequence)) {
            this.personalDetailsFragment.showPostcodeError();
        } else {
            this.personalDetailsFragment.hidePostcodeError();
        }
    }

    public final boolean validateInputsForButtonUpdate(String str, String str2, String str3) {
        CharSequence charSequence = str;
        boolean z = !(charSequence == null || StringsKt.isBlank(charSequence));
        CharSequence charSequence2 = str2;
        boolean z2 = !(charSequence2 == null || StringsKt.isBlank(charSequence2)) && isPostCodeValid(str2);
        CharSequence charSequence3 = str3;
        boolean z3 = !(charSequence3 == null || StringsKt.isBlank(charSequence3));
        if (!z || !z2 || !z3) {
            return false;
        }
        return true;
    }

    public final void validateInlinePostCode$app_release(String str) {
        CharSequence charSequence = str;
        if ((charSequence == null || charSequence.length() == 0) || str.length() != 4 || isPostCodeValid(str)) {
            this.personalDetailsFragment.hidePostcodeError();
        } else {
            this.personalDetailsFragment.showPostcodeError();
        }
    }

    private final boolean isPostCodeValid(String str) {
        return this.POST_CODE_REGEX.matcher(String.valueOf(str)).matches();
    }
}
