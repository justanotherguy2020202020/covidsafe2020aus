package okio.internal;

import com.google.common.base.Ascii;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import okio.Utf8;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0012\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u001e\u0010\u0003\u001a\u00020\u0002*\u00020\u00012\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005¨\u0006\u0007"}, d2 = {"commonAsUtf8ToByteArray", "", "", "commonToUtf8String", "beginIndex", "", "endIndex", "okio"}, k = 2, mv = {1, 1, 16})
/* compiled from: -Utf8.kt */
public final class _Utf8Kt {
    public static /* synthetic */ String commonToUtf8String$default(byte[] bArr, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = 0;
        }
        if ((i3 & 2) != 0) {
            i2 = bArr.length;
        }
        return commonToUtf8String(bArr, i, i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0097, code lost:
        if (((r0[r5] & 192) == 128) == false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x010f, code lost:
        if (((r0[r5] & 192) == 128) == false) goto L_0x0115;
     */
    public static final String commonToUtf8String(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        byte[] bArr2 = bArr;
        int i11 = i;
        int i12 = i2;
        Intrinsics.checkParameterIsNotNull(bArr2, "$this$commonToUtf8String");
        if (i11 < 0 || i12 > bArr2.length || i11 > i12) {
            throw new ArrayIndexOutOfBoundsException("size=" + bArr2.length + " beginIndex=" + i11 + " endIndex=" + i12);
        }
        char[] cArr = new char[(i12 - i11)];
        int i13 = 0;
        while (i11 < i12) {
            byte b = bArr2[i11];
            if (b >= 0) {
                i10 = i13 + 1;
                cArr[i13] = (char) b;
                i11++;
                while (i11 < i12 && bArr2[i11] >= 0) {
                    cArr[i10] = (char) bArr2[i11];
                    i11++;
                    i10++;
                }
            } else {
                if ((b >> 5) == -2) {
                    int i14 = i11 + 1;
                    if (i12 <= i14) {
                        i7 = i13 + 1;
                        cArr[i13] = (char) Utf8.REPLACEMENT_CODE_POINT;
                    } else {
                        byte b2 = bArr2[i11];
                        byte b3 = bArr2[i14];
                        if (!((b3 & 192) == 128)) {
                            i7 = i13 + 1;
                            cArr[i13] = (char) Utf8.REPLACEMENT_CODE_POINT;
                        } else {
                            byte b4 = (b3 ^ 3968) ^ (b2 << 6);
                            if (b4 < 128) {
                                i7 = i13 + 1;
                                cArr[i13] = (char) Utf8.REPLACEMENT_CODE_POINT;
                            } else {
                                i7 = i13 + 1;
                                cArr[i13] = (char) b4;
                            }
                            i8 = 2;
                            i11 += i8;
                        }
                    }
                } else if ((b >> 4) == -2) {
                    int i15 = i11 + 2;
                    if (i12 <= i15) {
                        i7 = i13 + 1;
                        cArr[i13] = (char) Utf8.REPLACEMENT_CODE_POINT;
                        int i16 = i11 + 1;
                        if (i12 > i16) {
                        }
                    } else {
                        byte b5 = bArr2[i11];
                        byte b6 = bArr2[i11 + 1];
                        if (!((b6 & 192) == 128)) {
                            i7 = i13 + 1;
                            cArr[i13] = (char) Utf8.REPLACEMENT_CODE_POINT;
                        } else {
                            byte b7 = bArr2[i15];
                            if (!((b7 & 192) == 128)) {
                                i7 = i13 + 1;
                                cArr[i13] = (char) Utf8.REPLACEMENT_CODE_POINT;
                                i8 = 2;
                                i11 += i8;
                            } else {
                                byte b8 = ((b7 ^ -123008) ^ (b6 << 6)) ^ (b5 << Ascii.FF);
                                if (b8 < 2048) {
                                    i9 = i13 + 1;
                                    cArr[i13] = (char) Utf8.REPLACEMENT_CODE_POINT;
                                } else if (55296 <= b8 && 57343 >= b8) {
                                    i9 = i13 + 1;
                                    cArr[i13] = (char) Utf8.REPLACEMENT_CODE_POINT;
                                } else {
                                    i9 = i13 + 1;
                                    cArr[i13] = (char) b8;
                                }
                                i8 = 3;
                                i11 += i8;
                            }
                        }
                    }
                } else {
                    if ((b >> 3) == -2) {
                        int i17 = i11 + 3;
                        if (i12 <= i17) {
                            i3 = i13 + 1;
                            cArr[i13] = Utf8.REPLACEMENT_CHARACTER;
                            int i18 = i11 + 1;
                            if (i12 > i18) {
                                if ((bArr2[i18] & 192) == 128) {
                                    int i19 = i11 + 2;
                                    if (i12 > i19) {
                                    }
                                    i5 = 2;
                                    i4 = i11 + i5;
                                }
                            }
                            i5 = 1;
                            i4 = i11 + i5;
                        } else {
                            byte b9 = bArr2[i11];
                            byte b10 = bArr2[i11 + 1];
                            if (!((b10 & 192) == 128)) {
                                i3 = i13 + 1;
                                cArr[i13] = Utf8.REPLACEMENT_CHARACTER;
                                i5 = 1;
                                i4 = i11 + i5;
                            } else {
                                byte b11 = bArr2[i11 + 2];
                                if (!((b11 & 192) == 128)) {
                                    i3 = i13 + 1;
                                    cArr[i13] = Utf8.REPLACEMENT_CHARACTER;
                                    i5 = 2;
                                    i4 = i11 + i5;
                                } else {
                                    byte b12 = bArr2[i17];
                                    if (!((b12 & 192) == 128)) {
                                        i3 = i13 + 1;
                                        cArr[i13] = Utf8.REPLACEMENT_CHARACTER;
                                    } else {
                                        byte b13 = (((b12 ^ 3678080) ^ (b11 << 6)) ^ (b10 << Ascii.FF)) ^ (b9 << Ascii.DC2);
                                        if (b13 > 1114111) {
                                            i6 = i13 + 1;
                                            cArr[i13] = Utf8.REPLACEMENT_CHARACTER;
                                        } else if (55296 <= b13 && 57343 >= b13) {
                                            i6 = i13 + 1;
                                            cArr[i13] = Utf8.REPLACEMENT_CHARACTER;
                                        } else if (b13 < 65536) {
                                            i6 = i13 + 1;
                                            cArr[i13] = Utf8.REPLACEMENT_CHARACTER;
                                        } else if (b13 != 65533) {
                                            int i20 = i13 + 1;
                                            cArr[i13] = (char) ((b13 >>> 10) + Utf8.HIGH_SURROGATE_HEADER);
                                            i6 = i20 + 1;
                                            cArr[i20] = (char) ((b13 & 1023) + Utf8.LOG_SURROGATE_HEADER);
                                        } else {
                                            i6 = i13 + 1;
                                            cArr[i13] = Utf8.REPLACEMENT_CHARACTER;
                                        }
                                        i5 = 4;
                                        i4 = i11 + i5;
                                    }
                                }
                            }
                        }
                        i5 = 3;
                        i4 = i11 + i5;
                    } else {
                        i3 = i13 + 1;
                        cArr[i13] = Utf8.REPLACEMENT_CHARACTER;
                        i4 = i11 + 1;
                    }
                    i13 = i3;
                }
                i8 = 1;
                i11 += i8;
            }
            i13 = i10;
        }
        return new String(cArr, 0, i13);
    }

    public static final byte[] commonAsUtf8ToByteArray(String str) {
        int i;
        int i2;
        int i3;
        char charAt;
        int i4;
        Intrinsics.checkParameterIsNotNull(str, "$this$commonAsUtf8ToByteArray");
        byte[] bArr = new byte[(str.length() * 4)];
        int length = str.length();
        int i5 = 0;
        while (i < length) {
            char charAt2 = str.charAt(i);
            if (charAt2 >= 128) {
                int length2 = str.length();
                int i6 = i;
                while (i < length2) {
                    char charAt3 = str.charAt(i);
                    if (charAt3 < 128) {
                        int i7 = i6 + 1;
                        bArr[i6] = (byte) charAt3;
                        i++;
                        while (i < length2 && str.charAt(i) < 128) {
                            bArr[i7] = (byte) str.charAt(i);
                            i++;
                            i7++;
                        }
                        i6 = i7;
                    } else {
                        if (charAt3 < 2048) {
                            int i8 = i6 + 1;
                            bArr[i6] = (byte) ((charAt3 >> 6) | 192);
                            i2 = i8 + 1;
                            bArr[i8] = (byte) ((charAt3 & '?') | 128);
                        } else if (55296 > charAt3 || 57343 < charAt3) {
                            int i9 = i6 + 1;
                            bArr[i6] = (byte) ((charAt3 >> 12) | 224);
                            int i10 = i9 + 1;
                            bArr[i9] = (byte) (((charAt3 >> 6) & 63) | 128);
                            i2 = i10 + 1;
                            bArr[i10] = (byte) ((charAt3 & '?') | 128);
                        } else if (charAt3 > 56319 || length2 <= (i3 = i + 1) || 56320 > (charAt = str.charAt(i3)) || 57343 < charAt) {
                            i2 = i6 + 1;
                            bArr[i6] = Utf8.REPLACEMENT_BYTE;
                        } else {
                            int charAt4 = ((charAt3 << 10) + str.charAt(i3)) - 56613888;
                            int i11 = i6 + 1;
                            bArr[i6] = (byte) ((charAt4 >> 18) | 240);
                            int i12 = i11 + 1;
                            bArr[i11] = (byte) (((charAt4 >> 12) & 63) | 128);
                            int i13 = i12 + 1;
                            bArr[i12] = (byte) (((charAt4 >> 6) & 63) | 128);
                            i2 = i13 + 1;
                            bArr[i13] = (byte) ((charAt4 & 63) | 128);
                            i4 = i + 2;
                            i6 = i2;
                        }
                        i4 = i + 1;
                        i6 = i2;
                    }
                }
                byte[] copyOf = Arrays.copyOf(bArr, i6);
                Intrinsics.checkExpressionValueIsNotNull(copyOf, "java.util.Arrays.copyOf(this, newSize)");
                return copyOf;
            }
            bArr[i] = (byte) charAt2;
            i5 = i + 1;
        }
        byte[] copyOf2 = Arrays.copyOf(bArr, str.length());
        Intrinsics.checkExpressionValueIsNotNull(copyOf2, "java.util.Arrays.copyOf(this, newSize)");
        return copyOf2;
    }
}
